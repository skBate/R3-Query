var ChartAction = {
	pieChart:function(id , title , showLegen , seriesData , enabled3d){
		$(id).highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: enabled3d,
					alpha: 45,
					beta: 0
				},
				plotBackgroundColor: null,
				plotBorderWidth: 1,//null,
				plotShadow: false
			},
			title: {
				text: title
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			exporting: {
				enabled: false
			},
			credits:{
				enabled:false
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					depth: enabled3d == true ? 35 : 0 ,
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>: {point.percentage:.1f} %',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					},
					point: {
                        events: {
                            click: function (e) {
                               
                            }
                        }
                    },
					showInLegend:showLegen
				}
			},
			series: seriesData
		});
	},
	lineChart:function(id , xAxis , title , seriesData){
		$(id).highcharts({
			chart: {
				type: 'spline'
			},
			title: {
				text: title
			},
			tooltip: {
				
			},
			exporting: {
				enabled: false
			},
			credits:{
				enabled:false
			},
			xAxis: xAxis,
			yAxis: {
				title: {
					text: title
				},
				min: 0,
				minorGridLineWidth: 0,
				gridLineWidth: 0,
				alternateGridColor: null
			},
			plotOptions: {
				spline: {
					lineWidth: 3,
					states: {
						hover: {
							lineWidth: 3
						}
					},
					marker: {
						enabled: false,
						symbol: 'circle',
						radius: 3,
						states: {
							hover: {
								enabled: true
							}
						}
					}
				}
			},
			series: seriesData,
			navigation: {
				menuItemStyle: {
					fontSize: '10px'
				}
			}
		});
	},
	areaChart:function(id , xAxis , title , seriesData){
		$(id).highcharts({
			chart: {
				type: 'areaspline'
			},
			title: {
				text: title
			},
			tooltip: {
				
			},
			exporting: {
				enabled: false
			},
			credits:{
				enabled:false
			},
			xAxis: xAxis,
			yAxis: {
				title: {
					text: title
				}
			},
			tooltip: {
				
			},
			plotOptions: {
				areaspline: {
					fillOpacity: 0.5,
					marker: {
						enabled: false,
						symbol: 'circle',
						radius: 3,
						states: {
							hover: {
								enabled: true
							}
						}
					}
				}
			},
			series: seriesData
		});
	},
	lineMicroChart:function(id , xAxis , title , seriesData){
		$(id).highcharts({
			chart: {
                backgroundColor: null,
                borderWidth:0,
                type: 'areaspline',
                margin: [2, 0, 2, 0],
                width: 100,
                height: 20,
                style: {
                    overflow: 'visible'
                },
                skipClone: true
            },
            title: {
                text: title
            },
            exporting: {
				enabled: false
			},
			credits:{
				enabled:false
			},
            xAxis: {
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                }
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false,
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                tickPositions: [0]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                backgroundColor: null,
                borderWidth: 0,
                shadow: false,
                useHTML: true,
                hideDelay: 0,
                shared: true,
                padding: 0,
                positioner: function (w, h, point) {
                    return { x: point.plotX - w / 2, y: point.plotY - h};
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    lineWidth: 2,
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        enabled: false,
                        radius: 1,
                        states: {
                            hover: {
                                radius: 3
                            }
                        }
                    },
                    fillOpacity: 0.25
                },
                column: {
                    negativeColor: '#910000',
                    borderColor: 'silver'
                }
            },
			series: seriesData
		});
	},
	mapChart:function(id , title , seriesData){
		option = {
			title : {
				text: 'iphone销量',
				subtext: '纯属虚构',
				x:'center'
			},
			tooltip : {
				trigger: 'item'
			},
			legend: {
				orient: 'vertical',
				x:'left',
				data:['iphone3','iphone4','iphone5']
			},
			dataRange: {
				min: 0,
				max: 2500,
				x: 'left',
				y: 'bottom',
				text:['高','低'],           // 文本，默认为数值文本
				calculable : true
			},
			toolbox: {
				show: true,
				orient : 'vertical',
				x: 'right',
				y: 'center',
				feature : {
					mark : {show: true},
					dataView : {show: true, readOnly: false},
					restore : {show: true},
					saveAsImage : {show: true}
				}
			},
			roamController: {
				show: true,
				x: 'right',
				mapTypeControl: {
					'china': true
				}
			},
			series : [
				{
					name: 'iphone3',
					type: 'map',
					mapType: 'china',
					roam: false,
					itemStyle:{
						normal:{label:{show:true}},
						emphasis:{label:{show:true}}
					},
					data:[
						{name: '北京',value: Math.round(Math.random()*1000)},
						{name: '天津',value: Math.round(Math.random()*1000)},
						{name: '上海',value: Math.round(Math.random()*1000)},
						{name: '重庆',value: Math.round(Math.random()*1000)},
						{name: '河北',value: Math.round(Math.random()*1000)},
						{name: '河南',value: Math.round(Math.random()*1000)},
						{name: '云南',value: Math.round(Math.random()*1000)},
						{name: '辽宁',value: Math.round(Math.random()*1000)},
						{name: '黑龙江',value: Math.round(Math.random()*1000)},
						{name: '湖南',value: Math.round(Math.random()*1000)},
						{name: '安徽',value: Math.round(Math.random()*1000)},
						{name: '山东',value: Math.round(Math.random()*1000)},
						{name: '新疆',value: Math.round(Math.random()*1000)},
						{name: '江苏',value: Math.round(Math.random()*1000)},
						{name: '浙江',value: Math.round(Math.random()*1000)},
						{name: '江西',value: Math.round(Math.random()*1000)},
						{name: '湖北',value: Math.round(Math.random()*1000)},
						{name: '广西',value: Math.round(Math.random()*1000)},
						{name: '甘肃',value: Math.round(Math.random()*1000)},
						{name: '山西',value: Math.round(Math.random()*1000)},
						{name: '内蒙古',value: Math.round(Math.random()*1000)},
						{name: '陕西',value: Math.round(Math.random()*1000)},
						{name: '吉林',value: Math.round(Math.random()*1000)},
						{name: '福建',value: Math.round(Math.random()*1000)},
						{name: '贵州',value: Math.round(Math.random()*1000)},
						{name: '广东',value: Math.round(Math.random()*1000)},
						{name: '青海',value: Math.round(Math.random()*1000)},
						{name: '西藏',value: Math.round(Math.random()*1000)},
						{name: '四川',value: Math.round(Math.random()*1000)},
						{name: '宁夏',value: Math.round(Math.random()*1000)},
						{name: '海南',value: Math.round(Math.random()*1000)},
						{name: '台湾',value: Math.round(Math.random()*1000)},
						{name: '香港',value: Math.round(Math.random()*1000)},
						{name: '澳门',value: Math.round(Math.random()*1000)}
					]
				},
				{
					name: 'iphone4',
					type: 'map',
					mapType: 'china',
					itemStyle:{
						normal:{label:{show:true}},
						emphasis:{label:{show:true}}
					},
					data:[
						{name: '北京',value: Math.round(Math.random()*1000)},
						{name: '天津',value: Math.round(Math.random()*1000)},
						{name: '上海',value: Math.round(Math.random()*1000)},
						{name: '重庆',value: Math.round(Math.random()*1000)},
						{name: '河北',value: Math.round(Math.random()*1000)},
						{name: '安徽',value: Math.round(Math.random()*1000)},
						{name: '新疆',value: Math.round(Math.random()*1000)},
						{name: '浙江',value: Math.round(Math.random()*1000)},
						{name: '江西',value: Math.round(Math.random()*1000)},
						{name: '山西',value: Math.round(Math.random()*1000)},
						{name: '内蒙古',value: Math.round(Math.random()*1000)},
						{name: '吉林',value: Math.round(Math.random()*1000)},
						{name: '福建',value: Math.round(Math.random()*1000)},
						{name: '广东',value: Math.round(Math.random()*1000)},
						{name: '西藏',value: Math.round(Math.random()*1000)},
						{name: '四川',value: Math.round(Math.random()*1000)},
						{name: '宁夏',value: Math.round(Math.random()*1000)},
						{name: '香港',value: Math.round(Math.random()*1000)},
						{name: '澳门',value: Math.round(Math.random()*1000)}
					]
				},
				{
					name: 'iphone5',
					type: 'map',
					mapType: 'china',
					itemStyle:{
						normal:{label:{show:true}},
						emphasis:{label:{show:true}}
					},
					data:[
						{name: '北京',value: Math.round(Math.random()*1000)},
						{name: '天津',value: Math.round(Math.random()*1000)},
						{name: '上海',value: Math.round(Math.random()*1000)},
						{name: '广东',value: Math.round(Math.random()*1000)},
						{name: '台湾',value: Math.round(Math.random()*1000)},
						{name: '香港',value: Math.round(Math.random()*1000)},
						{name: '澳门',value: Math.round(Math.random()*1000)}
					]
				}
			]
		};
							
		var echart = echarts.init(document.getElementById(id));
		echart.setOption(option);
	},
	gaugeChart:function(id , pkgid , title , seriesData , param){
		$(id).highcharts({
				chart: {
				type: 'gauge',
				plotBackgroundColor: null,
				plotBackgroundImage: null,
				plotBorderWidth: 0,
				plotShadow: false
			},

			title: {
				text: title
			},
			exporting: {
				enabled: false
			},
			credits:{
				enabled:false
			},
			pane: {
				startAngle: -150,
				endAngle: 150,
				background: [{
					backgroundColor: {
						linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
						stops: [
							[0, '#FFF'],
							[1, '#333']
						]
					},
					borderWidth: 0,
					outerRadius: '109%'
				}, {
					backgroundColor: {
						linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
						stops: [
							[0, '#333'],
							[1, '#FFF']
						]
					},
					borderWidth: 1,
					outerRadius: '107%'
				}, {
					// default background
				}, {
					backgroundColor: '#DDD',
					borderWidth: 0,
					outerRadius: '105%',
					innerRadius: '103%'
				}]
			},

			// the value axis
			yAxis: {
				min: param.min,
				max: param.max,

				minorTickInterval: 'auto',
				minorTickWidth: 1,
				minorTickLength: 10,
				minorTickPosition: 'inside',
				minorTickColor: '#666',

				tickPixelInterval: 30,
				tickWidth: 2,
				tickPosition: 'inside',
				tickLength: 10,
				tickColor: '#666',
				labels: {
					step: 2,
					rotation: 'auto'
				},
				title: {
					text: param.title
				},
				plotBands: [{
					from: 0,
					to: 60,
					color: '#55BF3B' // green
				}, {
					from: 60,
					to: 90,
					color: '#DDDF0D' // yellow
				}, {
					from: 90,
					to: 100,
					color: '#DF5353' // red
				}]
			},

			series: seriesData
		});
	},
	funnelChart:function(id , pkgid , title , seriesData , param){
		$(id).highcharts({
			chart: {
				type: 'funnel',
				marginRight: 100
			},

			title: {
				text: title
			},
			exporting: {
				enabled: false
			},
			credits:{
				enabled:false
			},
			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b> ({point.y:,.0f})',
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
						softConnector: true
					},
					neckWidth: '30%',
					neckHeight: '25%'
				}
			},	 
			series: seriesData
		});
	}
}
function createChartDisplay(target , type , name  , data , param){
	$(target).highcharts({
		chart: {
			type: type,
			marginRight: (param.marginright && param.marginright!=null) ?  param.marginright : 100
		},
		exporting: {
			enabled: false
		},
		title: {
			text: param.displaytitle ? param.title : "",
			x: -50
		},
		credits:{
			enabled:false
		},
		plotOptions: {
			series: {
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b> {point.y:,.0f}',
					color: 'black',
					softConnector: true
				},
				neckWidth: param.neckwidth,
				neckHeight: param.neckheight,
				events: {  
					click: function(event) {
						if(param.click){
							param.click();
						}
					}  
				}  
				//-- Other available options
				// height: pixels or percent
				// width: pixels or percent
			}
		},
		legend: {
			enabled: false
		},
		series: [{
			data: data
		}]
	});
}

function createModelChart(target , type , name  , data , param){
	$(target).highcharts({
        chart: {
            type: 'column'
        },
        exporting: {
			enabled: false
		},
		title: {
			text:'',
			x: -50
		},
		credits:{
			enabled:false
		},
        xAxis: {
            categories: data.name
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span>',
            pointFormat: '' + '',
            footerFormat: '<table><tbody><tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.1f} mm</b></td></tr></tbody></table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: data.data
    });
}