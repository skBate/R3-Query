/**
* 光标放在最后 $("#文本框ID").textFocus();
* 光标放在第二个字符后面 $("#文本框ID").textFocus(2);
*/
(function($){
	$.fn.textFocus=function(v){
		var isNotIE=-[-1,];
			
		var range,len,v=v===undefined?0:parseInt(v);
		this.each(function(){
			if(isNotIE){
				len=this.value.length;
				v===0?this.setSelectionRange(len,len):this.setSelectionRange(v,v);
			}else{
				range=this.createTextRange();
				v===0?range.collapse(false):range.move("character",v);
				range.select();					
			}
			this.focus();
		});
		return this;
	}
})(jQuery)

function getStartPos( textarea ) 
{ 
	if ( typeof textarea.selectionStart != 'undefined' ) 
	{ // 非IE 
		start = textarea.selectionStart; 
	} 
	else 
	{ // IE 
		var range = document.selection.createRange(); 
		var range_textarea = document.body.createTextRange(); 
		range_textarea .moveToElementText(textarea); 
		//比较start point 
		for ( var sel_start = 0; range_textarea .compareEndPoints('StartToStart' , range) < 0; sel_start++) 
		range_textarea .moveStart( 'character', 1); 
		start = sel_start; 
	} // else 

	return start; 
} 
var curModelId = "";
var BtnAction = {
	selRowData:"default",
	save:function(){
		ReportAction.saveQueryRerport(url_list.save_url);
		tipCloseWindow = false ;
		tipCloseWindowMessage = "" ;
	},
	saveas:function(){
		art.prompt('请输入另存的报表名称' , function(value){
			ReportAction.saveAsQueryRerport(url_list.saveas_url , value)
		});
	},
	clear:function(){
		art.confirm('您确定需要清空当前已设计的报表内容？' , function(){
			ReportAction.clearQueryReport(url_list.clear_url);
		} , function(){
		
		}) ;		
	},
	publish:function(){
		tipArtPageWithLock('发布报表',url_list.publish , 650 , false);	
	},
	clearStyle:function(id){
		art.confirm('您确定需要清除当前已设置的格式？' , function(){BtnAction.setstyle('clear','' , id)} , function(){})		
	},
	deletecol:function(){
		ReportAction.deleteQueryReportCol();
	},
	mergecell:function(type){
		ReportAction.mergeQueryReportCell(url_list.merge_url  , type);
	},
	clearmerge:function(){
		ReportAction.clearMergeQueryReportCell(url_list.clearmerge_url);
	},
	newcol:function(){
		ReportAction.queryReportNewCol(url_list.newcol_url);
	},
	newRow:function(){
		ReportAction.queryReportNewRow(url_list.newrow_url);
	},
	exchange:function(){
		ReportAction.queryReportExchange(url_list.exchange_url);
	},
	exviewtype:function(){
		ReportAction.exchangeViewType(url_list.exchange_viewtype);
	},
	exchartype:function(){
		ReportAction.exchangeChartType(url_list.exchange_chartype);
	},
	setstyle:function(key , value , dataid , dataType){
		ReportAction.setStyle(url_list.style_url , key , value , dataid , dataType);
	},
	setMicrometer:function(){
		ReportAction.setMicrometer(url_list.micrometer_url);
	},
	setColSize:function(colSize){
		loadURL(url_list.setColSize+"?colSize="+colSize,"#design_table");
	},
	drilldef:function(e){
		return ReportAction.setDrilDef(e , url_list.drilldef_url);
	},
	clearzero:function(){
		ReportAction.clearZeroQueryReport(url_list.clearzero_url);
	},
	operators:function(obj , operator){
		if(curCurPos >= 0){
			$(obj).val($(obj).val().substring(0 , curCurPos) + operator + $(obj).val().substring(curCurPos , $(obj).val().length ));
		}else{
			if($(obj).val().length > 0){
				$(obj).val($(obj).val() + " " + operator);
			}else{
				$(obj).val($(obj).val() + operator);
			}
			curCurPos = $(obj).val().length ;
		}
		curCurPos = curCurPos + operator.length ;
		$(obj).textFocus(curCurPos);
	},
	chart:function(e){
		ReportAction.setChart(e , url_list.charts_url);
	},
	warning:function(e){
		ReportAction.setWarning(e , url_list.warning_url);
	},
	addFilter:function(e){
		tipArtPage("添加过滤器" , url_list.filterparam_url,750);
		//ReportAction.addFilter(e , url_list.filterparam_url);
	},
	viewQuery:function(e){
		tipArtPage("设定查询语句" , url_list.viewquery_url+"?p="+$('#p').val()+"&ps="+$('#ps').val(),750);
	},
	countView:function(e){
		tipArtPage("报表数据统计" , url_list.countview_url,850);
	},
	setParityline:function(obj){
		if(currentParityline == "%"){
			currentParityline = "0" ;
			$(obj).attr("title","偶数行");
			$(obj).text("0");
		}else if(currentParityline == "0"){
			currentParityline = "1" ;
			$(obj).attr("title","奇数行");
			$(obj).text("1");
		}else if(currentParityline == "1"){
			currentParityline = "%" ;
			$(obj).attr("title","所有行");
			$(obj).text("%");
		}
	},
	addNewPackage:function(templetid , pkgid , pkgpos , chartdiv){
		loadURL(url_list.addnewpackage+"?templetid="+templetid+"&pkgid="+pkgid+"&pkgpos="+pkgpos,chartdiv);
	},
	loadProperties:function(pkgid , chartdiv){
		loadURL(url_list.properties+"?pkgid="+pkgid,chartdiv);
	},
	loadLayoutProperties:function(layoutid , pkgid , chartdiv){
		loadURL(url_list.layoutproperties+"?layoutid="+layoutid+"&pkgid="+pkgid,chartdiv);
	},
	addNewPackageLayout:function(templetid , pkgid , rows, cols , chartdiv){
		loadURL(url_list.addnewpackage+"?templetid="+templetid+"&pkgid="+pkgid+"&rows="+rows+"&cols="+cols,chartdiv);
	},
	addLayoutNewRow:function(pkgid , chartdiv){
		loadURL(url_list.addlayoutnewrow+"?pkgid="+pkgid,chartdiv);
	},
	addLayoutNewCol:function(pkgid , chartdiv){
		loadURL(url_list.addlayoutnewcol+"?pkgid="+pkgid,chartdiv);
	},
	addChartNewYAxis:function(pkgid , chartdiv){
		loadURL(url_list.addnewyaxis+"?pkgid="+pkgid,chartdiv);
	},
	addChartNewSeries:function(pkgid , chartdiv){
		loadURL(url_list.addnewseries+"?pkgid="+pkgid,chartdiv);
	},
	setXAxisData:function(pkgid , name , chartdiv){
		loadURL(url_list.setxaxisdata+"?pkgid="+pkgid + "&name="+name,chartdiv);
	},
	colpos:function(name , div){
		loadURL(url_list.colpos_url+"?tl="+encodeURIComponent(name),div);
	},
	setYAxisData:function(pkgid , name , datainx , chartdiv){
		loadURL(url_list.addnewyaxis+"?pkgid="+pkgid + "&name="+name+"&datainx="+datainx,chartdiv);
	},
	deleteYAxisData:function(pkgid , id , chartdiv){
		loadURL(url_list.deleteyaxis+"?pkgid="+pkgid + "&id="+id,chartdiv);
	},
	delPackage:function(pkgid , modeldiv){
		art.confirm("删除元素会同时删除元素的配置信息，请确认是否删除元素？",function(){
			loadURL(url_list.deletepackage+"?pkgid="+pkgid,modeldiv);		
		});			
	},
	delTaskNode:function(taskid){
		art.confirm("删除任务节点会同时删除节点的配置信息，请确认是否删除任务节点？",function(){
			location.href = url_list.deletetasknode+"?taskid="+taskid;		
		},function(){});			
	},
	delPackageLayout:function(pkgid , layoutpkgid , modeldiv){
		art.confirm("删除元素会同时删除元素的配置信息，请确认是否删除元素？",function(){
			loadURL(url_list.deletepackage+"?pkgid="+pkgid+"&layoutpkgid="+layoutpkgid,modeldiv);		
		});			
	},
	editNewRowCol:function(e){
		ReportAction.editRowOrCol(e , url_list.editroworcol);
	},
	setColRename:function(e){
		ReportAction.setColRename(e , url_list.colrename);
	},
	setHiddencol:function(e){
		ReportAction.setHiddencol(e , url_list.hiddencol);
	},
	editMeasureGroup:function(e){
		ReportAction.editMeasureGroup(e , url_list.edit_measure_group);
	},
	syncReportModelMeasure:function(obj , mid,func){
		if($(obj).attr("data-check")=="false"){
			$(obj).attr("data-check","true");
			ReportAction.syncReportModelMeasure(url_list.design_url+"?type=measure&oid="+mid+"&pos=right",func);
		}else{
			$(obj).attr("data-check","false");
			ReportAction.syncReportModelMeasure(url_list.design_rmmeasure_url+"?type=measure&oid="+mid+"&pos=right",func);
		}
	},
	chartvalue:function(treeNode , column , inx , chartdiv){
		  ReportAction.setChartValue(url_list.chartvalue_url + "?pkgid="+treeNode.pkgid+"&serise=true&id="+treeNode.sid+"&name="+column +"&datainx="+inx , chartdiv);
	},
	chartvaluerange:function(treeNode , column , inx , start , end , chartdiv){
		  ReportAction.setChartValue(url_list.chartvalue_url + "?pkgid="+treeNode.pkgid+"&serise=true&id="+treeNode.sid+"&name="+column +"&datainx="+inx+"&start="+start+"&end="+end , chartdiv);
	}
};


var ReportAction = {
	tipOperInfo:function(data){
		var d = art.dialog({
			id:"reportActionDialog",
			time:500,
			content:data
		});
	},
	close:function(data){
		art.dialog.list["reportActionDialog"].close();
	},
	saveQueryRerport: function(url){
		loadURL(url,"#tempdiv",function(data){
			ReportAction.tipOperInfo(data);
			if(window.opener !=null && curModelId!=null && window.opener.$("#comp_"+curModelId).length > 0){
				window.opener.reloadModel(url_list.analytics_model , "#comp_"+curModelId);
			}
		});
	},	
	saveAsQueryRerport: function(url , value){
		url = url + "?name="+encodeURIComponent(value);
		loadURL(url,"#tempdiv",function(data){
			ReportAction.tipOperInfo(data);			
		});
	},	
	clearZeroQueryReport: function(url){
	  	 loadURL(url,"#design_table",function(data){
			//ReportAction.tipOperInfo("。");
		});
	},
	clearQueryReport: function(url){
	  	 loadURL(url,"#design_table",function(data){
			ReportAction.tipOperInfo("清空报表所选项成功。");
		});
	},
	deleteQueryReportCol: function(){
		if(selColDataID!=null && selColDataID!=""){
			var url = null ;
			
			if(selColDataID.attr('data-flag')=="measure"){	  				
				url = url_list.del_col_url+"?title="+selColDataID.attr("data-title")+"&type="+selColDataID.attr("data-flag");
			}else if(selColDataID.attr('data-flag')=="newcol"){
				url = url_list.rmnewcol_url+"?id="+selColDataID.attr("data-id")+"&type="+selColDataID.attr("data-flag");
			}else if(selColDataID.attr('data-flag')=="newrow"){
				url = url_list.rmnewrow_url+"?id="+selColDataID.attr("data-id")+"&type="+selColDataID.attr("data-flag");
			}else{
				url = url_list.cleardim_url+"?title="+selColDataID.attr("data-title")+"&type="+selColDataID.attr("data-flag");
			}
			loadURL(url,"#design_table",function(data){
				ReportAction.tipOperInfo("删除所选列成功。");
			});
		}else{
			ReportAction.tipOperInfo("无选中列，请先选中后再删除。");
		}
	},
	editRowOrCol:function(e , url){
		if(selColDataID!=null && selColDataID!=""){
			if(selColDataID.attr('data-id')){
				tipArtPageWithLock('修改汇总行/列',url+"?dataid="+selColDataID.attr('data-id') , 750 , false);	
			}else{
				ReportAction.tipOperInfo("当前选中项不是汇总行或列。");	
			}
		}else{
			ReportAction.tipOperInfo("无选中汇总行或列，请先选中后再修改。");
		}
	},
	setColRename:function(e , url){	
		tipArtPageWithLock('重命名',url, 400 , false);			
	},
	setHiddencol:function(e , url){	
		tipArtPageWithLock('隐藏指定列',url, 400 , false);			
	},
	editMeasureGroup:function(url){	
		tipArtPageWithLock('编辑指标分组',url, 400 , false);			
	},
	mergeQueryReportCell: function(url , type){
		if(cellmergestart!=null && cellmergestart!="" && cellmergend!=null && cellmergend!=""){
			url = url+"?start="+cellmergestart+"&end="+cellmergend+"&mt="+type+"&format="+encodeURIComponent($('#mergeformat').val());
			loadURL(url,"#design_table",function(data){
				ReportAction.tipOperInfo("合并单元格成功。");
			});
		}else{
			ReportAction.tipOperInfo("无选中单元格，请先选中后再合并。");
		}
	},
	clearMergeQueryReportCell: function(url){
		if($(".ui-selected").length == 1 && $(".ui-selected").attr("data-cellmerge")!=null){
			url = url+"?mergeid="+$(".ui-selected").attr("data-cellmerge");
			loadURL(url,"#design_table",function(data){
				ReportAction.tipOperInfo("取消合并单元格成功。");
			});
		}else{
			ReportAction.tipOperInfo("未选中已合并单元格，请先选中后再取消。");
		}
	},
	queryReportNewCol: function(url){
	  	 loadURL(url,"#design_table",function(data){
			ReportAction.tipOperInfo("新增列成功。");
		});
	},
	queryReportNewRow: function(url){
	  	 loadURL(url,"#design_table",function(data){
			ReportAction.tipOperInfo("新增行成功。");
		});
	},
	queryReportExchange: function(url){
	  	 loadURL(url,"#design_table",function(data){
			//ReportAction.tipOperInfo("行列转换成功。");
		});
	},
	exchangeViewType: function(url){
	  	art.confirm("将图表转换为表格会丢失图表格式信息，请确认？",function(){
			loadURL(url,"#design_table",function(data){
				
			});			
		});
		
	},
	syncReportModelMeasure: function(url,func){
	  	 loadURL(url,"#design_table",function(data){
			//ReportAction.tipOperInfo("行列转换成功。");func
	  		 if(func!=null){
	  			 func();
	  		 }
		});
	},
	
	exchangeChartType: function(url){
		loadURL(url,"#design_table",function(data){
			closeR3ArtDialog();
		});
	},
	setStyle: function(url , key , value , dataid , dataType){
		if(selColDataID!=null && selColDataID!=""){
			dataType = 	"row" ;
			dataid =  selColDataID.attr("data-pos");
		}else{
			dataType = "model" ;
		}
		url = url + "?key="+key+"&value="+encodeURIComponent(value)+"&dataid="+dataid+"&datatype="+dataType+"&pline="+encodeURIComponent(currentParityline);
		loadURL(url,"#design_table",function(data){
			closeR3ArtDialog();
		});
	},
	setMicrometer: function(url){
		if(selColDataID!=undefined){
			var title =  selColDataID.attr("data-title");
			if(title!=undefined){
				url = url + "?title="+encodeURIComponent(title);
				loadURL(url,"#design_table",function(data){
					closeR3ArtDialog();
				});
			}
		}else{
			art.alert("抱歉，请选择要格式化为千分位的列。");
		}
		
		
	},
	setWarning: function(e , url){
		var name = "";
		if(selColDataID!=null && selColDataID!=""){
			name =  selColDataID.attr("data-title");
		}else{
			ReportAction.tipOperInfo("未选中需要预警的指标列，请先选中后再定义。");
			return ;
		}
		tipArtPage("设置预警项", url + "?name="+name, 750);
	},
	setDrilDef: function(e , url){
		if(selColDataID!=null && selColDataID!=""){
			//$(e).attr("data-toggle","ajax");
			$(e).attr("href" , url+"?title="+selColDataID.attr("data-title")+"&type="+selColDataID.attr("data-flag"));
			return true;
		}else{
			ReportAction.tipOperInfo("未选中钻取列，请先选中后再定义。");
			return false ;
		}
	},
	setChart: function(e , url){
		if(cellmergestart!=null && cellmergestart!="" && cellmergend!=null && cellmergend!=""){
			//$(e).attr("href" , url+"?start="+cellmergestart+"&end="+cellmergend);
		}else{
			ReportAction.tipOperInfo("未选中数据集，请先选中后再定义。");
			return false ;
		}
	},
	setChartValue: function(url  , chartdiv){
		loadURL(url,chartdiv,function(data){
			
		});	
	},
	loadMeasureGroup:function(obj){
		loadURL(url_list.group_measure_url,obj);
	}
};

var KeyEvent = {
	init:function(){
		KeyEvent.save(url_list.save_url);	//按键保存
		KeyEvent.delcol();					//按键删除选中列
	},
	initChartEvent:function(){
		KeyEvent.closeRightPanel();
		KeyEvent.closeBottomPanel();
	},
	save:function(url){
		$("body").bind('keydown', 'ctrl+s',function (evt){
			BtnAction.save();
			return false;
		});
	},
	delcol:function(){
		$(document).bind('keydown', 'del',function (evt){
			BtnAction.deletecol();
			return false;
		});
	},
	closeRightPanel:function(){
		$(document).bind('keydown', 'ctrl+r',function (evt){
			//BtnAction.deletecol();
			$('#compInxDiv').click();
			return false;
		});
	},
	closeBottomPanel:function(){
		$(document).bind('keydown', 'ctrl+b',function (evt){
			$('#dtInxDiv').click();
			return false;
		});
	}
};

