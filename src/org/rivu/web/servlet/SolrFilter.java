package org.rivu.web.servlet;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.rivues.core.RivuDataContext;

public class SolrFilter extends org.apache.solr.servlet.SolrDispatchFilter{
	
	public void init(FilterConfig config) throws ServletException
	{
		super.init(config) ;
		RivuDataContext.setCorecontainer(super.getCores()) ;
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	    super.doFilter(request, response, chain, false);
	}
	/**
	 * 释放资源
	 */
	public void destroy() {
	    super.destroy();  
	}
}
