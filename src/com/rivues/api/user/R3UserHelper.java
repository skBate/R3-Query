package com.rivues.api.user;

import org.apache.commons.httpclient.NameValuePair;

import com.rivues.api.util.R3HttpTools;
import com.rivues.api.util.StringUtil;

public class R3UserHelper {
	private String url;
	private String authzUser;
	private String authzPassword;
	
	public R3UserHelper() {
	}
	public R3UserHelper(String url, String authzUser, String authzPassword) {
		super();
		this.url = url;
		this.authzUser = authzUser;
		this.authzPassword = authzPassword;
	}
	
	public R3User createR3User(R3User user)throws Exception{
		if(StringUtil.isBlank(url)){
			throw new Exception("url不能为空。");
		}
		
		if(StringUtil.isBlank(authzUser)){
			throw new Exception("authzUser不能为空。");
		}
		
		if(StringUtil.isBlank(authzPassword)){
			throw new Exception("authzPassword不能为空。");
		}
		
		if(user==null){
			throw new Exception("user不能为空。");
		}
		
		if(StringUtil.isBlank(user.getUsername())){
			throw new Exception("user.username不能为空。");
		}
		if(StringUtil.isBlank(user.getPassword())){
			throw new Exception("user.password不能为空。");
		}
		if(StringUtil.isBlank(user.getEmail())){
			throw new Exception("user.email不能为空。");
		}
		if(StringUtil.isBlank(user.getMobile())){
			throw new Exception("user.mobile不能为空。");
		}
		NameValuePair[] data = {new NameValuePair("username",user.getUsername())
		,new NameValuePair("password",user.getPassword())
		,new NameValuePair("email",user.getEmail())
		,new NameValuePair("nickname",user.getNickname())
		,new NameValuePair("mobile",user.getMobile())
		,new NameValuePair("authzUser",authzUser)
		,new NameValuePair("authzPassword",authzPassword)};
		String result = R3HttpTools.sendHttp(url, data);
		if("authentication_faild".equals(result)){
			throw new Exception("该用户没通过R3系统认证，请更换修正用户名或密码。");
		}else if(result.indexOf("usersize")>=0){
			throw new Exception("用户已存在，请更换用户名。");
		}else{
			user.setId(result);
		
		}
		return user;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAuthzUser() {
		return authzUser;
	}
	public void setAuthzUser(String authzUser) {
		this.authzUser = authzUser;
	}
	public String getAuthzPassword() {
		return authzPassword;
	}
	public void setAuthzPassword(String authzPassword) {
		this.authzPassword = authzPassword;
	}
	
}
