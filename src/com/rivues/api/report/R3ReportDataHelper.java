package com.rivues.api.report;

import org.apache.commons.httpclient.NameValuePair;

import com.rivues.api.util.R3HttpTools;
import com.rivues.api.util.StringUtil;

/**
 * 给第三方提供r3的数据接口
 * @author Administrator
 *
 */
public class R3ReportDataHelper {
	private String url;
	private String authzUser;
	private String authzPassword;
	
	public R3ReportDataHelper() {
	}
	
	public R3ReportDataHelper(String url, String authzUser, String authzPassword) {
		super();
		this.url = url;
		this.authzUser = authzUser;
		this.authzPassword = authzPassword;
	}
	public String findReportData(String reportid,int datacount) throws Exception{
		String dataStr = null;
	
		if(StringUtil.isBlank(url)){
			throw new Exception("url不能为空。");
		}
		
		if(StringUtil.isBlank(authzUser)){
			throw new Exception("authzUser不能为空。");
		}
		
		if(StringUtil.isBlank(authzPassword)){
			throw new Exception("authzPassword不能为空。");
		}
		
		NameValuePair[] data = {new NameValuePair("reportid",reportid)
		,new NameValuePair("datacount",datacount+"")
		,new NameValuePair("authzUser",authzUser)
		,new NameValuePair("authzPassword",authzPassword)};
		String result = R3HttpTools.sendHttp(url, data);
		if("authentication_faild".equals(result)){
			throw new Exception("该用户没通过R3系统认证，请更换修正用户名或密码。");
		}if("noreport".equals(result)){
			throw new Exception("输入的报表id在R3中不存在，请输入正确的报表id。");
		}else{
			dataStr = result;
		}
		return dataStr;
	}


	
	
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAuthzUser() {
		return authzUser;
	}
	public void setAuthzUser(String authzUser) {
		this.authzUser = authzUser;
	}
	public String getAuthzPassword() {
		return authzPassword;
	}
	public void setAuthzPassword(String authzPassword) {
		this.authzPassword = authzPassword;
	}
}
