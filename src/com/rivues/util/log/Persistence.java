package com.rivues.util.log;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.rivues.module.platform.web.model.Log;
import com.rivues.module.platform.web.model.QueryLog;
import com.rivues.module.platform.web.model.SystemLog;
import com.rivues.util.log.impl.DBLogPersistence;

public abstract class Persistence {
	/**
	 * 按租户 分页 获取获取 LOG4J记录的日志列表，即查询 RIVU_BusLog表，默认按时间排序，默认不区分日志类型，如果传入的 type 不为空，则按照 type 过滤日志，获取所有日志
	 * @param orgi					组合ID，系统日志为 SYSTEM
	 * @param level					日志级别
	 * @param flowid				点击流程ID
	 * @param startid				系统启动ID
	 * @param start
	 * @param ps
	 * @return
	 */
	public abstract List<Log> getBusLogList(String orgi ,String level , String flowid , String startid , int start , int ps) ;
	
	/**
	 * 根据条件查询日志分页
	 * @param criteria查询条件
	 * @param start
	 * @param ps
	 * @return
	 */
	public abstract List<Log> getBusLogPage(DetachedCriteria criteria, int start, int ps);
	
	/**
	 * 根据开始条数和页面大小获取分页数据
	 * @param criteria
	 * @param start
	 * @param ps
	 * @return
	 */
	public abstract List<Log> getBusLogPageByIndex(DetachedCriteria criteria, int start, int ps);
	/**
	 * 获取log数量
	 * @param criteria
	 * @return
	 */
	public abstract int getLogCount(DetachedCriteria criteria);
	
	
	/**
	 * 按 租户分页 获取 系统日志记录列表 ， 例如，  启动、停止 系统等，默认按时间排序，默认不区分日志类型，如果传入的 type 不为空，则按照 type 过滤日志，获取所有日志
	 * @param type
	 * @param num
	 * @return
	 */
	public abstract List<SystemLog> getSystemLogList(String orgi ,  String type , int start , int ps) ;
	
	/**
	 * 按租户 分页 获取操作日志，默认按时间排序，例如，报表点击操作、页面点击操作等
	 * @param type
	 * @param num
	 * @return
	 */
	public abstract List<QueryLog> getRequestLogList(String orgi , String level , int start , int ps) ;
	
	
	private static Persistence persistence = null ;
	/**
	 * 
	 * @return
	 */
	public static Persistence getLogPersistence(){
		return persistence ==null ? persistence = new DBLogPersistence() : persistence;
	}
	
}
