package com.rivues.util.service.cache.impl.ehcache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import com.rivues.core.RivuDataContext;
import com.rivues.util.RivuTools;
import com.rivues.util.service.cache.CacheBean;
import com.rivues.util.service.monitor.BusinessService;

public class EhCacheInstance extends BusinessService implements CacheBean{
	
	private String cacheName ;					//缓存名称
	
	
	private Cache getCache(){
		CacheManager  cacheManage = (CacheManager ) RivuDataContext.getWebApplicationContext().getBean("cacheManage") ;
		return cacheManage.getCache(cacheName);
	}
	
	public EhCacheInstance getCacheInstance(String cacheName){
		this.cacheName = cacheName ;
		return this ;
	}
	
	
	@Override
	public void put(String key, Object value, String orgi) {
		getCache().put(new Element(key, value)) ;
	}

	@Override
	public void clear(String orgi) {
		getCache().removeAll();
	}

	@Override
	public void delete(String key, String orgi) {
		getCache().remove(key) ;
	}

	@Override
	public void update(String key, String orgi, Object value) {
		this.delete(key, orgi) ;
		this.put(key, value, orgi) ;
	}
	@Deprecated
	@Override
	public Object getCacheObject(String key, String orgi) {
		Object obj = getCache().get(key)!=null ? getCache().get(key).getObjectValue() : null  ;
		if(obj!=null){
			try {
				obj = RivuTools.toObject(RivuTools.toBytes(obj)) ;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return obj;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return EhCacheHelper.CacheServiceEnum.EHCACHE_CACHE.toString();
	}

//	@Override
	public void service() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Collection<?> getAllCacheObject(String orgi) {
		List valueList = new ArrayList() ;
		List cacheValue = this.getCache().getKeys();
		for(Object key : cacheValue){
			if(this.getCache().get(key)!=null){
				valueList.add(this.getCache().get(key).getObjectValue());
			}
		}
		return valueList;
	}
	@Override
	public Object getCacheObject(String key, String orgi, Object defaultValue) {
		// TODO Auto-generated method stub
		Object obj = this.getCacheObject(key, orgi);
		if(obj==null){
			obj = defaultValue;
		}
		return obj;
	}


}
