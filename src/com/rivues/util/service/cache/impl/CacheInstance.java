package com.rivues.util.service.cache.impl;

import mondrian.olap.Connection;

import com.rivues.util.service.cache.CacheBean;

public interface CacheInstance {
	/**
	 * 获取通用缓存，报表对象缓存
	 * @return
	 */
	public CacheBean getDistributedCacheBean() ;
	
	/**
	 * 字典缓存
	 * @return
	 */
	public CacheBean getDistributedDictionaryCacheBean();
	
	/**
	 * 报表对象缓存
	 * @return
	 */
	public CacheBean getDistributedReporyDataCacheBean();
	
	/**
	 * Session
	 * @return
	 */
	public CacheBean getDistributedSessionCacheBean();
	
	
	/**
	 * 获取查询维度上的 聚合 指标缓存
	 * @return
	 */
	public CacheBean getMDXAggCacheBean() ;
	
	/**
	 * 获取集群环境中的 服务器状态
	 * @return
	 */
	public CacheBean getClusterServerCacheBean() ;
	
}