package com.rivues.util.service.business;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;

import com.rivues.core.RivuDataContext;
import com.rivues.util.service.data.RequestStaticisBean;
import com.rivues.util.service.monitor.BusinessService;

/**
 * 
 * @author admin
 *
 */
@Aspect
@Service
public class StaticisService{
	
	@Pointcut("execution(* com.rivues.util.service.*.*.service(..))")
    public void service(String orgi){};
    
    @Around("execution(* com.rivues.util.service.*.*.service(..))")
    public Object staticis(ProceedingJoinPoint point) throws Throwable{
    	/**
    	 * 服务器已启动，并且 未停止
    	 */
    	Object object = null ;
    	if(RivuDataContext.isStart() && !RivuDataContext.isStopping()){
	    	/**
	    	 * 需要将 统计的 服务属性注入
	    	 */
	    	BusinessService service = (BusinessService)point.getTarget() ;
	    	if(RivuDataContext.getStaticServiceMap().get(service.getName())==null){
	    		RivuDataContext.getStaticServiceMap().put(service.getName(), new RequestStaticisBean(service.getName()));
	    	}
	    	RequestStaticisBean staticisBean = RivuDataContext.getStaticServiceMap().get(service.getName()) ;
	    	long start = System.currentTimeMillis() ;
	    	long times = 0 ;
	    	try{
	    		object = point.proceed();
	    		times = System.currentTimeMillis() - start ;
	    		staticisBean.setSuccessrequest(1) ;
	    		staticisBean.setSuccesstime(times) ;
	    		if(times > staticisBean.getSuccessmaxtime()){
	    			staticisBean.setSuccessmaxtime(times) ;
	    		}
	    		if(times < staticisBean.getSuccessmintime() || staticisBean.getSuccessmintime() == 0){
	    			staticisBean.setSuccessmintime(times) ;
	    		}
	    		
	    	} catch (Throwable e) {
				times = System.currentTimeMillis() - start ;
				staticisBean.setFaildrequest(1) ;
				staticisBean.setFaildtime(times) ;
				if(times > staticisBean.getFaildmaxtime()){
					staticisBean.setFaildmaxtime(times) ;
				}
				if(times < staticisBean.getFaildmintime() || staticisBean.getFaildmintime() == 0 ){
					staticisBean.setFaildmintime(times) ;
				}
				throw e;
			}finally{
				staticisBean.setServicetimes(1) ;
				staticisBean.setRunningtime(times) ;
				if(times > staticisBean.getMaxrequesttime()){
					staticisBean.setMaxrequesttime(times) ;
				}
				if(staticisBean.getMinrequesttime()==0 || times < staticisBean.getMinrequesttime()){
					staticisBean.setMinrequesttime(times) ;
				}
			}
    	}
    	return object ;
    }
    @Around("execution(* com.rivues.util.service.cache.impl.*.*.put(..))")
    public Object cacheStaticsPut(ProceedingJoinPoint point) throws Throwable{
    	return staticis(point);
    }
    @Around("execution(* com.rivues.util.service.cache.impl.*.*.clear(..))")
    public Object cacheStaticsClear(ProceedingJoinPoint point) throws Throwable{
    	return staticis(point);
    }
    @Around("execution(* com.rivues.util.service.cache.impl.*.*.delete(..))")
    public Object cacheStaticsDelete(ProceedingJoinPoint point) throws Throwable{
    	return staticis(point);
    }
    @Around("execution(* com.rivues.util.service.cache.impl.*.*.update(..))")
    public Object cacheStaticsUpdate(ProceedingJoinPoint point) throws Throwable{
    	return staticis(point);
    }
    @Around("execution(* com.rivues.util.service.cache.impl.*.*.getCacheObject(..))")
    public Object cacheStaticsUpdateGetCacheObject(ProceedingJoinPoint point) throws Throwable{
    	Object object = staticis(point);
    	if (object==null) {
    		BusinessService service = (BusinessService)point.getTarget() ;
	    	if(RivuDataContext.getStaticServiceMap().get(service.getName())==null){
	    		RivuDataContext.getStaticServiceMap().put(service.getName(), new RequestStaticisBean(service.getName()));
	    	}
	    	RequestStaticisBean staticisBean = RivuDataContext.getStaticServiceMap().get(service.getName()) ;
	    	staticisBean.setCacheNotFound(1);
		}
    	return object;
    }
}
