package com.rivues.util.service.business;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.AnalyzerReport;
import com.rivues.module.platform.web.model.AnalyzerReportModel;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.Database;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.QueryLog;
import com.rivues.module.platform.web.model.ReportFilter;
import com.rivues.util.data.ReportData;
import com.rivues.util.datasource.DataSource;
import com.rivues.util.iface.cube.CubeFactory;
import com.rivues.util.serialize.JSON;
import com.rivues.util.service.ServiceHelper;
import com.rivues.util.service.monitor.BusinessService;
import com.rivues.util.service.report.DataSourceTools;
import com.rivues.util.tools.ExportFile;

@Service("report")
public class ReportService extends BusinessService{
	/**
	 * 报表服务
	 * @param request
	 * @param model
	 * @param report
	 * @param parseParam
	 * @return
	 * @throws Exception 
	 */
	public ReportData service(HttpServletRequest request , AnalyzerReportModel model , AnalyzerReport report , boolean parseParam , QueryLog queryLog , boolean cache , boolean updateCache) throws Exception{
		/**
		 * model 的 Cube 为null ，通过 published获取Cube , Cube存在于缓存中
		 */
		ReportData reportData = null ;
		PublishedCube publishedCube = CubeFactory.getCubeInstance().getCubeByID(model.getPublishedcubeid()) ;
		if(publishedCube!=null){
			Cube cube = publishedCube.getCube(RivuDataContext.getUser(request)) ;
			reportData = ServiceHelper.getReportDataFactoryService().service(request , publishedCube , cube, model, report, parseParam, queryLog , cache , updateCache) ;
		}
		return reportData;
	}
	/**
	 * 获取导出数据的类型
	 * @param request
	 * @param model
	 * @param report
	 * @param parseParam
	 * @param queryLog
	 * @param cache
	 * @param updateCache
	 * @return
	 * @throws Exception
	 */
	public String getDsType(HttpServletRequest request , AnalyzerReportModel model , AnalyzerReport report , boolean parseParam , QueryLog queryLog , boolean cache , boolean updateCache) throws Exception{
		PublishedCube publishedCube = CubeFactory.getCubeInstance().getCubeByID(model.getPublishedcubeid()) ;
		String dsType = publishedCube.getDstype() ;
		if(publishedCube!=null){
			Cube cube = publishedCube.getCube() ;
			if(cube!=null){
				dsType = cube.getDstype() ;
			}
		}
		return dsType ;
	}
	
	/**
	 * 获取导出数据的类型
	 * @param request
	 * @param model
	 * @param report
	 * @param parseParam
	 * @param queryLog
	 * @param cache
	 * @param updateCache
	 * @return
	 * @throws Exception
	 */
	public Database getDatabaseType(HttpServletRequest request , AnalyzerReportModel model , AnalyzerReport report , boolean parseParam , QueryLog queryLog , boolean cache , boolean updateCache) throws Exception{
		PublishedCube publishedCube = CubeFactory.getCubeInstance().getCubeByID(model.getPublishedcubeid()) ;
		String dsType = publishedCube.getDstype() ;
		Database database = null ;
		if(publishedCube!=null){
			Cube cube = publishedCube.getCube() ;
			if(cube!=null){
				database = (Database) RivuDataContext.getService().getIObjectByPK(Database.class, cube.getDb()) ;
			}
		}
		return database ;
	}
	
	/**
	 * 专为导出服务
	 * @param request
	 * @param model
	 * @param report
	 * @param parseParam
	 * @return
	 * @throws Exception 
	 */
	public void service(HttpServletRequest request , AnalyzerReportModel model , AnalyzerReport report , boolean parseParam , QueryLog queryLog ,ExportFile export) throws Exception{
		PublishedCube publishedCube = CubeFactory.getCubeInstance().getCubeByID(model.getPublishedcubeid()) ;
		if(publishedCube!=null){
			/**
			 * 查询之前，需要把 Report的 Filter 传给 Model
			 */
			model.setFilters(new ArrayList<ReportFilter>()) ;
			//model.getFilters().addAll(report.getFilters()) ;
			for (ReportFilter filter : report.getFilters()) {//把不是分组的过滤器填充到model中
				
				if(filter.getGroupids()==null||(filter.getGroupids()!=null&&filter.getGroupids().trim().length()==0)){
					boolean flag = true;
					if(RivuDataContext.ReportCompareEnum.COMPARE.toString().equals(filter.getValuefiltertype())){
						if(StringUtils.isBlank(filter.getDefaultvalue())&&StringUtils.isBlank(filter.getRequestvalue())){
							flag=false;
						}
					}else{
						if(StringUtils.isBlank(filter.getDefaultvalue())&&StringUtils.isBlank(filter.getRequeststartvalue())&&StringUtils.isBlank(filter.getRequestendvalue())){
							flag=false;
						}
					}
					if(flag){
						model.getFilters().add(filter);
					}
					
				}
			}
			Cube cube = publishedCube.getCube() ;
			DataSource ds = DataSourceTools.getDataSource(model.getDstype(), cube) ;
			ds.getRSData(model, report.getRequestParamValues(), cube, request, parseParam,  ds.getQuery(model , cube, request , parseParam , queryLog) , export);
		}
	}
	
	/**
	 * 报表服务
	 * @param request
	 * @param model
	 * @param report
	 * @param parseParam
	 * @return
	 * @throws Exception 
	 */
	public ReportData service(HttpServletRequest request , AnalyzerReportModel model , Cube cube , AnalyzerReport report , boolean parseParam , QueryLog queryLog , boolean cache , boolean updateCache) throws Exception{
		/**
		 * model 的 Cube 为null ，通过 published获取Cube , Cube存在于缓存中
		 */
		PublishedCube publishedCube = CubeFactory.getCubeInstance().getCubeByID(model.getPublishedcubeid()) ;
		return ServiceHelper.getReportDataFactoryService().service(request , publishedCube , cube, model, report, parseParam, queryLog , cache , updateCache) ;
	}
	/**
	 * 默认服务
	 */
	public void service(){
		
	}
	
	@Override
	public String getName() {
		return RivuDataContext.ServiceTypeName.REPORT.toString();
	}
}
