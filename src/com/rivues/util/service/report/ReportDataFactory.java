package com.rivues.util.service.report;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import mondrian.rolap.BitKey.Big;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.util.SerializationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.googlecode.aviator.AviatorEvaluator;
import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.AnalyzerReport;
import com.rivues.module.platform.web.model.AnalyzerReportModel;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.CubeLevel;
import com.rivues.module.platform.web.model.CubeMeasure;
import com.rivues.module.platform.web.model.Dimension;
import com.rivues.module.platform.web.model.DrillDown;
import com.rivues.module.platform.web.model.MeasureGroup;
import com.rivues.module.platform.web.model.MeasureGroupData;
import com.rivues.module.platform.web.model.MeasureWarningMeta;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.QueryLog;
import com.rivues.module.platform.web.model.QueryText;
import com.rivues.module.platform.web.model.RenameCol;
import com.rivues.module.platform.web.model.ReportCellMerge;
import com.rivues.module.platform.web.model.ReportCol;
import com.rivues.module.platform.web.model.ReportFilter;
import com.rivues.module.platform.web.model.User;
import com.rivues.module.platform.web.model.Warning;
import com.rivues.util.RivuTools;
import com.rivues.util.data.FirstTitle;
import com.rivues.util.data.Level;
import com.rivues.util.data.ModelData;
import com.rivues.util.data.ReportData;
import com.rivues.util.data.ValueData;
import com.rivues.util.datasource.DataSource;
import com.rivues.util.iface.report.R3Request;
import com.rivues.util.serialize.JSON;
import com.rivues.util.service.ServiceHelper;
import com.rivues.util.service.cache.CacheHelper;
import com.rivues.util.service.monitor.BusinessService;
import com.rivues.util.service.system.DataPersistenceService;
import com.rivues.util.tools.RivuReportDataUtil;
import com.rivues.util.tools.StringUtil;

import freemarker.template.TemplateException;

@Service("reportdatafactory")
public class ReportDataFactory  extends BusinessService{
	private final Logger log = LoggerFactory.getLogger(ReportDataFactory.class); 
	/**
	 * 为Model 赋值 , cache 参数 为 是否启用报表缓存，如果为 false ， 则表示不启用缓存
	 * @param model
	 * @return
	 * @throws FileNotFoundException
	 */
	public ReportData service(HttpServletRequest request , PublishedCube publishedCube , Cube cube , AnalyzerReportModel model , AnalyzerReport report ,boolean parseParam  , QueryLog queryLog , boolean cache , boolean updateCache) throws Exception{
		
		ReportData reportData = null;
		long queryTime = 0 ;
		if(parseParam){
			if(request.getParameter("fl")!=null){
				model.setFilterstr(request.getParameter("fl")) ;
			}
			if(request.getParameter("tl")!=null){
				model.setTitle(request.getParameter("tl")) ;
			}
			if(request.getParameter("s")!=null){
				model.setStart(request.getParameter("s")) ;
			}
			if(request.getParameter("e")!=null){
				model.setEnd(request.getParameter("e")) ;
			}
		}
		if(model.getCode()!=null && model.getCode().length()>0){
			StringBuffer strb = new StringBuffer().append(model.getCode()).append(".fl") ;
			String filter = "" ;
			if(parseParam){
				if(request.getParameter(strb.toString())!=null){
					filter = request.getParameter(strb.toString()) ;
				}
			}
			if((filter!=null && filter.length()>0) || (model.getFilterstr()!=null && model.getFilterstr().length()>0)){
				strb = new StringBuffer();
				strb.append("(").append(filter) ;
				if(filter!=null && filter.length()>0){
					model.setFilterstr(strb.append(",").append(model.getFilterstr()).append(")").toString()) ;
				}else{
					model.setFilterstr(strb.append(model.getFilterstr()).append(")").toString()) ;
				}
			}
		}
		long start = System.currentTimeMillis() ;
		/**
		 * 获取数据源
		 */
		DataSource dataSource = DataSourceTools.getDataSource(model.getDstype(), cube) ;
		/**
		 * 查询之前，需要把 Report的 Filter 传给 Model
		 */
		Object filterstyle =  CacheHelper.getDistributedDictionaryCacheBean().getCacheObject("system.config.report.CubeFilterStyle", "rivues") ;
		model.setFilters(new ArrayList<ReportFilter>()) ;
		//model.getFilters().addAll(report.getFilters()) ;
		for (ReportFilter filter : report.getFilters()) {//把不是分组的过滤器填充到model中
			
			
			
			if(!StringUtils.isBlank(filter.getCurvalue())
					&&filter.getCurvalue().trim().startsWith("[")
					&&filter.getCurvalue().trim().endsWith("]")
					&&filterstyle!=null&&filterstyle.toString().equals("false")){//
				String[] strs = filter.getCurvalue().split(RivuDataContext.DEFAULT_VALIUE_SPLIT) ;
				StringBuffer appendbuff = new StringBuffer();
				for (String string : strs) {
					if(appendbuff.length()>0){
						appendbuff.append(RivuDataContext.DEFAULT_VALIUE_SPLIT);
					}
					appendbuff.append(string.substring(string.indexOf(".")+2, string.length()-1));
					
				}
				filter.setDefaultvalue(appendbuff.toString());
			}
			
			if(filter.getGroupids()==null||(filter.getGroupids()!=null&&filter.getGroupids().trim().length()==0)){
				boolean flag = true;
				if(RivuDataContext.ReportCompareEnum.COMPARE.toString().equals(filter.getValuefiltertype())){
					if(StringUtils.isBlank(filter.getCurvalue())){
						flag=false;
					}
				}else{
					if(StringUtils.isBlank(filter.getCurstartvalue())&&StringUtils.isBlank(filter.getCurendvalue())){
						flag=false;
					}
				}
				if(flag){
					model.getFilters().add(filter);
				}
				
			}
		}
		if(report.getRequestParamValues()==null){
			report.setRequestParamValues(new HashMap<String, Object>());
		}
		report.getRequestParamValues().put("filters", model.getFilters());
		/**
		 * MD5作为 缓存的KEY
		 */
		QueryText query = dataSource.getQuery(model , cube, request , parseParam  , queryLog) ;
		if(queryLog!=null){
			queryLog.setQueryparsetime(System.currentTimeMillis() - start) ;
		}
		/**
		 * 设置 查询语句
		 */
		String key = query!=null && query.getQueryText()!=null ? RivuTools.md5(query.getQueryText()) : null;
		query.setKey(key) ;
//		model.setQuerytext(null) ;
		
		/**
		 * 以下代码开始获取模型访问日志
		 */
		QueryLog cubeQueryLog = null;
		if(queryLog!=null){
			cubeQueryLog = queryLog.clone();
			cubeQueryLog.setStarttime(new Date()) ;
		}
		/**
		 * 2014-05-06 21:31增加了 updateCache选项，用户手动更新缓存的时候使用该参数
		 */
		if(updateCache || !cache || cube.getSql()!=null || (!(request instanceof R3Request) && report.isLazy() && !report.isLoadata()) || (key==null || (reportData = (ReportData)CacheHelper.getDistributedReporyDataCacheBean().getCacheObject(key , null))==null)){//key==null || (reportData = (ReportData)CacheHelper.getCacheBean().getCacheObject(key , null))==null
			
			if(cubeQueryLog!=null){
				cubeQueryLog.setDetailtype(RivuDataContext.DataFillTypeEnum.DATASOURCE.toString()) ;
			}
			/**
			 * 以下是在设置数据源组件的情况下，先从数据源组件获取数据
			 */
			if(model.getDsmodel()!=null && model.getDsmodel().length()>0){
				for(AnalyzerReportModel arm : report.getModel()){
					if(arm.getId().equals(model.getDsmodel())){
						if(arm.getReportData()==null){
							arm.setReportData(service(request ,publishedCube ,  cube , arm, report , parseParam , queryLog , cache , updateCache)) ;
						}
						reportData = dataSource.getData(model , report.getRequestParamValues() , cube , request,parseParam , dataSource.getQuery(model , cube, request , parseParam , queryLog) , queryLog) ;
						break ;
					}
				}
				try{
					ReportData currentReportData = dataSource.getData(model ,report.getRequestParamValues() , cube , request,parseParam , query , queryLog) ;
					
					if(currentReportData!=null && reportData!=null){
						/**
						 * 是否进行行列转换，去掉行列转换，改为在CubeTools中处理
						 */
						//						if(model.isExchangerw()){
						//							currentReportData.exchangeColRow() ;
						//						}
						reportData.merge(currentReportData) ;
					}
				}catch(Exception ex){
					if(cubeQueryLog!=null){
						cubeQueryLog.setThrowable(ex.getMessage()!=null ? ex.getMessage().length()<200 ? ex.getMessage() : ex.getMessage().substring(0, 200) : "") ;
						cubeQueryLog.setError("true") ;
					}
					if(reportData==null){
						reportData = new ModelData() ;
						model.setReportData(reportData) ;
					}
					reportData.setException(ex) ;
					ex.printStackTrace();
					throw ex ;
				}
			}else{//如果未设置数据源，则从自定义数据模型获取数据
				try{
					//如果参数设定为默认打开不加载数据源，则在默认打开的时候加载一个模拟的数据源，不进行查询，在点击查询按钮的时候再发送请求道数据库
					/**
					 * 
					 */
					if(!(request instanceof R3Request) && report.isLazy() && !report.isLoadata()){
						reportData = dataSource.getVData(model , report.getRequestParamValues(), cube , request,parseParam , query , queryLog) ;
					}else{
						reportData = dataSource.getData(model , report.getRequestParamValues(), cube , request,parseParam , query , queryLog) ;
					}
					if(model.getGroupList().size() > 0){
						String modelMeasure = model.getMeasure();
						String mdx = model.getQuerytext();
						for(MeasureGroup mgroup : model.getGroupList()){
							model.setFilters(new ArrayList<ReportFilter>());
							for (ReportFilter filter : report.getFilters()) {//把不是分组的过滤器填充到model中
								if(filter.getGroupids()!=null&&filter.getGroupids().indexOf(mgroup.getId())>-1){
									boolean flag = true;
									if(RivuDataContext.ReportCompareEnum.COMPARE.toString().equals(filter.getValuefiltertype())){
										if(StringUtils.isBlank(filter.getCurvalue())){
											flag=false;
										}
									}else{
										if(StringUtils.isBlank(filter.getCurstartvalue())&&StringUtils.isBlank(filter.getCurendvalue())){
											flag=false;
										}
									}
									if(flag){
										model.getFilters().add(filter);
									}
								}
							}
							StringBuffer sb = new StringBuffer();
							if(mgroup.getMeasures()!=null){
								for (MeasureGroupData measure : mgroup.getMeasures()) {
									if(sb.toString().length()>0){
										sb.append(",");
									}
									sb.append(measure.getMeasureid());
								}
								model.setMeasure(sb.toString());
							}
							
							model.setQuerytext(null);
							if(mgroup.getMdx()!=null && mgroup.getMdx().trim().length()>0){
								model.setQuerytext(mgroup.getMdx());
							}
							QueryText tempQuery = dataSource.getQuery(model , cube, request , parseParam  , queryLog) ;
							ReportData tempReportData = dataSource.getData(model  , report.getRequestParamValues(), cube , request,parseParam , tempQuery , queryLog) ;
							/**
							 * 最后再处理 重命名
							 */
							RivuReportDataUtil.processReplaceGroupMeasureName(model, tempReportData);
							
							reportData.merge(tempReportData);
						}
						model.setMeasure(modelMeasure);
						model.setQuerytext(mdx);
					}
					
					/**
					 * 是否进行行列转换，去掉行列转换，改为在CubeTools中处理
					 */
//					if(model.isExchangerw()){
//						reportData.exchangeColRow() ;
//					}
				}catch(Exception ex){
					if(cubeQueryLog!=null){
						cubeQueryLog.setThrowable(ex.getMessage()!=null ? ex.getMessage().length()<200 ? ex.getMessage() : ex.getMessage().substring(0, 200) : "") ;
						cubeQueryLog.setError("true") ;
					}
					if(reportData==null){
						reportData = new ModelData() ;
						model.setReportData(reportData) ;
					}
					ex.printStackTrace();
					reportData.setException(ex) ;
					throw ex;
				}
			}
			
			if(cube!=null && !RivuDataContext.CubeEnum.CUBE.toString().equals(cube.getDstype()) && reportData!=null){
				if(reportData.getCol()!=null && reportData.getCol().getChilderen().size()==1){
					reportData.getCol().getChilderen().remove(0);
					for(List<ValueData> valueList : reportData.getData()){
						for(ValueData value : valueList){
							reportData.getCol().getChilderen().add(new Level(String.valueOf(value.getValue()) , reportData.getCol().getLeveltype() , reportData.getCol() ,2)) ;
						}
					}
				}
			}
			
			reportData.setDate(new Date());//记录数据查询时间
			/**
			 * 记录缓存
			 */
			if(dataSource!=null && reportData!=null && cache){
				if(!(request instanceof R3Request) && report.isLazy() && !report.isLoadata()){ //1、报表配置了 默认不加载数据 ； 2、未点击查询按钮  ； 目前只有Player页面 有此功能
					//加载虚拟数据，不缓存
				}else{
					/**
					 * 缓存数据，需要和用户的角色关联，避免出现未授权数据访问  , 放入缓存前，先克隆一份
					 */
					CacheHelper.getDistributedReporyDataCacheBean().put(key, SerializationHelper.clone(reportData) , null) ;
				}
			}
			log.info("数据获取时间：" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " 用时：" + (System.currentTimeMillis() - start)) ;
		}else{
			/**
			 * 重新从缓存里 获取数据的时候 ， 克隆一份数据
			 */
			reportData = (ReportData) SerializationHelper.clone(reportData) ;
			
			log.info("从缓存获取数据时间："+ new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " 用时：" + (System.currentTimeMillis() - start)) ;
			if(cubeQueryLog!=null){
				cubeQueryLog.setDetailtype(RivuDataContext.DataFillTypeEnum.CACHE.toString()) ;
			}
		}
		if(reportData!=null){
			if(request.getParameter("p")!=null && request.getParameter("p").matches("[\\d]{1,}")){
				reportData.setPage(Integer.parseInt(request.getParameter("p"))) ;
			}
			if(request.getParameter("ps")!=null && request.getParameter("ps").matches("[\\d]{1,}")){
				reportData.setPageSize(Integer.parseInt(request.getParameter("ps"))) ;
			}
		}
//		start = System.currentTimeMillis() ;
		/**
		 * 以下代码用于记录模型的数据访问日志，并记录模型的 名称 等信息
		 */
		if(cubeQueryLog!=null){
			cubeQueryLog.setDatatype(RivuDataContext.QueryLogEnum.MODEL.toString()) ;
			//cubeQueryLog.setReportdic(publishedCube.getDiclocation()) ;
			cubeQueryLog.setDataid(cube.getId()) ;
			cubeQueryLog.setDataname(cube.getName()) ;
			cubeQueryLog.setEndtime(new Date()) ;
			cubeQueryLog.setQuerytime(cubeQueryLog.getEndtime().getTime()-cubeQueryLog.getStarttime().getTime());
			/**
			 * 将日志 加入到 异步日志记录信息
			 */
			DataPersistenceService.persistence(cubeQueryLog) ;
			/**  日志记录结束 **/
		}
		queryTime = System.currentTimeMillis() - start ;
		if(reportData==null){
			reportData = new ModelData() ;
			model.setReportData(reportData) ;
		}
		reportData.setQueryTime(queryTime) ;
		/**
		 * 隐藏所选列
		 */
		RivuReportDataUtil.processHiddencol(model, reportData);
		
		/**
		 * 如果没数据把维度标题填充上
		 */
		RivuReportDataUtil.processFullFirstTitle(model, reportData);
		
		if(reportData.getData()!=null && reportData.getData().size()>0){//表格格式处理
			/**
			 * 清理 ReportData 的表头信息
			 */
			RivuReportDataUtil.cleanTitle(model , reportData);
			/**
			 * 清理 ReportData
			 */
			RivuReportDataUtil.clearReportData(model , reportData) ;
			
			/**
			 * 处理单元格合并
			 */
			RivuReportDataUtil.processCellMerge(model , reportData) ;
			
			
			
			
			
			
			/**
			 * 优先处理 新增的行列，避免 出现 单元格操作的上海 越界
			 */
			if(model.getColformatstr()!=null && model.getColformatstr().length()>0){
				List<Object> colList = JSON.parseObject(model.getColformatstr(), List.class) ;
				if(colList!=null && colList.size()>0){
					/**
					 * 先处理所有列
					 */
					RivuReportDataUtil.processNewCol(model , reportData , colList) ;
					
					/**
					 * 然后处理所有行
					 */
					RivuReportDataUtil.processNewRow(model , reportData , colList , cube) ;
						
					/**
					 * 最后再处理 重命名
					 */
					RivuReportDataUtil.processRename(model , reportData , colList) ;
				}
			}
			
			/**
			 * 处理预警项
			 */
			RivuReportDataUtil.processWarning(model, reportData);
			/**
			 * 千分位
			 */
			RivuReportDataUtil.processMicrometer(model, reportData);
			
			/**
			 * 处理 标题描述
			 */
			RivuReportDataUtil.processTitileDescription(reportData , model,cube);
			/**
			 * 处理 钻取
			 */
			reportData = RivuReportDataUtil.processDrillDown(request,reportData ,report, model);
			
			model.setReportData(reportData) ;
			
			
//			System.out.println("处理表头后");
//			for (FirstTitle f : model.getReportData().getRow().getFirstTitle()) {
//				System.out.println("表头的name值为："+f.getName()+"    表头的Rename的值为:"+f.getRename());
//			}
			
		}
		
		/**
		 * 如果是图表 ， 填充图表数据 ， 填充方式为 Freemarker 模板 ， 以下部分是数据渲染服务部分，4.0的渲染方式
		 */
		if(reportData!=null && model.getModeltype()!=null){
			Map<String , Object> values = new HashMap<String, Object>();
			Enumeration<String> param = request.getParameterNames() ;
			Map<String , String> params = new HashMap() ;
			while(param.hasMoreElements()){
				String name = param.nextElement() ;
				if(request.getParameter(name)!=null && request.getParameter(name).trim().length()>0){
					values.put(name, request.getParameter(name)) ;
					params.put(name , request.getParameter(name)) ;
				}
			}
			int p = Integer.parseInt(request.getParameter("p") !=null && request.getParameter("p").matches("[\\d]{1,}") ? request.getParameter("p") :"1");
			int ps = Integer.parseInt(request.getParameter("ps") !=null && request.getParameter("ps").matches("[\\d]{1,}") ? request.getParameter("ps") :"20");
			if(p<1){
				p = 1;
			}
			if(ps<1){
				ps =20 ;
			}
			
			values.put("startindex", (p-1) * ps) ;
			values.put("params", params) ;
			values.put("request",request) ;
			values.put("model", model) ;
			values.put("data", reportData) ;
			if(values.get("start")==null && model.getStart()!=null && model.getStart().length()>0){
				model.setStart(RivuTools.getTemplet(model.getStart(), values)) ;
				values.put("start", model.getStart()) ;
			}
			if(values.get("end")==null && model.getEnd()!=null && model.getEnd().length()>0){
				model.setEnd(RivuTools.getTemplet(model.getEnd(), values)) ;
				values.put("end", model.getEnd()) ;
			}
			if(StringUtil.isNotNull(model.getDefaultvalue())&& model.getDefaultvalue().indexOf("$")!=-1){
				model.setDefaultvalue(RivuTools.getTemplet(model.getDefaultvalue(), values)) ;
			}
			if(model.getDrilldown()!=null){
				for(DrillDown drillDown : model.getDrilldown()){
					if(drillDown.getParamvalue()!=null && drillDown.getParamvalue().toString().indexOf("$")>=0){
						drillDown.setParamvalue(dataSource.processParam(drillDown.getParamvalue() , cube , model, request, parseParam , null)) ;	
					}
				}
			}
			if(model.getPagesetting()!=null && model.getPagesetting().getWidth()!=null && model.getPagesetting().getWidth()!=""&&!"".equals(model.getPagesetting().getWidth())){
				String widthStr = model.getPagesetting().getWidth();
				String[] widthStrs = widthStr.split(";;");
				if(widthStrs.length>0){
					for(String str : widthStrs){
						if(str.indexOf("::")!=-1){
							model.getPagesetting().getWidths().put(str.split("::")[0], str.split("::")[1]);
						}
					}
				}
			}
			List <List<MeasureWarningMeta>> measureWarnings = new ArrayList<List<MeasureWarningMeta>>();
			if(StringUtil.isNotNull(model.getMeasure())){
			for(String measureID : model.getMeasure().split(","))
			{
				
				List <MeasureWarningMeta> measureWarning = RivuDataContext.getService().findAllByCriteria(DetachedCriteria.forClass(MeasureWarningMeta.class).add(Restrictions.eq("dataid",measureID)).add(Restrictions.eq("modelid", model.getId())).add(Restrictions.eq("orgi", model.getOrgi()))) ;
				if(measureWarning.size()>0)
				{
					measureWarnings.add(measureWarning);
				}
			}
			}
			values.put("measureWarns", measureWarnings);
			
			
			model.setReportData(reportData) ;
			reportData.setQueryText(query) ;
			/**
			 * 渲染 页面数据，4.0功能，
			 * 已发现的问题：
			 * 		1、在一次渲染的数据量大了以后，会导致内存溢出
			 */
//			reportData.setViewData(RivuTools.getTemplet(RivuDataContext.getSearchResultTemplet(model.getModeltype()), values)) ;
			/**
			 * //移除掉 request ，避免缓存的时候序列化失败 , 以下代码 智能在  模板渲染完成之后 
			 */
			values.remove("request");	
			reportData.setOptions(values) ;
		}
		if(reportData!=null){
			reportData.setQueryText(query) ;
		}
		if(queryLog!=null){
			queryLog.setDataformatime(System.currentTimeMillis()-start) ;
		}
		return reportData ;
	}
	/**
	 * 处理数据的FirstTitle , 用于获取模型的 dataid
	 * @param cube
	 * @param reportData
	 */
	private void processDataID(Cube cube , ReportData reportData){
		if(reportData!=null && reportData.getCol()!=null && reportData.getCol().getFirstTitle()!=null){
			for(FirstTitle title : reportData.getCol().getFirstTitle()){
				for(CubeMeasure measure : cube.getMeasure()){
					
				}
			}
		}
	}

	/**
	 * 格式化
	 * @param formatstr
	 * @param title
	 * @return
	 */
	private void formattitle(String format , Level level , HttpServletRequest request , List<List<ValueData>> dataValueList , ReportData reportData){
		String[] formatstr = format.split(",") ;
		if(level.getTitle().size()>0 && level.getTitle().get(level.getTitle().size()-1).size()==0){
			level.getTitle().remove(level.getTitle().size()-1);
		}
		for(String fst : formatstr){
			String[] rowfst = fst.split(":",4) ;
			if(rowfst.length==4){
				if(rowfst[0].matches("[\\d]*") ){
					String[] dt = rowfst[1].trim().split("/") ;
					if(dt.length>0 && dt[0].matches("[\\d]*")){
						int rowindex = Integer.parseInt(rowfst[0].trim()) ;
						rowindex = rowindex>0 ? rowindex : 1 ;
						int colindex = Integer.parseInt(dt[0].trim()) ;
						if(level.getTitle().size()>rowindex){
							List rowtitle = level.getTitle().get(rowindex) ;
							if("none".equals(rowfst[2])){
								if((rowindex)<level.getTitle().size() && level.getFirstTitle().size()>(rowindex)){
									level.getTitle().remove(rowindex) ;
									level.getFirstTitle().remove(rowindex) ;
									level.setDepth(level.getDepth()-1) ;
								}
							}else{
								if(rowtitle.size()>colindex-1){
									if(colindex>0 && dt.length<=1){
										Level title = (Level) rowtitle.get(colindex-1) ;
										{
											if(rowfst[2].trim().equals("number") && title.getNameValue().matches("[\\d]*") ){
												title.setName(String.valueOf(Integer.parseInt(title.getNameValue()))) ;
											}
											title.setName(regexReplace( rowfst[3].replaceAll("<data>", title.getNameValue()) , request)) ;
										}
									}else{
										int allindex = Integer.parseInt(dt.length==1?dt[0]:dt[1]) ;
										for(int i=0 ; i<rowtitle.size();i++){
											if(colindex==0 || i%allindex==colindex){
												Level title = (Level)rowtitle.get(i);
												if(rowfst[2].equals("number") && title.getNameValue().matches("[\\d]*") ){
													title.setName(String.valueOf(Integer.parseInt(title.getNameValue()))) ;
												}
												if(title!=null){
													title.setName(regexReplace (rowfst[3].replaceAll("<data>", title.getNameValue()) , request)) ;
												}
											}
											
										}
									}
								}
							}
						}else{	//超出列范围，格式化数据
							int dataindex = rowindex - level.getTitle().size() ;
							if("row".equals(level.getLeveltype())){
								if(dataindex<dataValueList.size()){
									if("none".equals(rowfst[2])){
										for(List<ValueData> vd : dataValueList){
											if(vd.size()>dataindex){
												vd.remove(dataindex);
											}
										}
										if(reportData.getCol()!=null && reportData.getCol().getTitle()!=null && dataindex < reportData.getCol().getTitle().size()){
											List<Level> titleList = reportData.getCol().getTitle().get(reportData.getCol().getTitle().size()-1) ;
											if(dataindex<titleList.size()){
												titleList.remove(dataindex);
											}
										}
									}else{
										List<ValueData> valueList = dataValueList.get(dataindex) ;
										for(ValueData data : valueList){
											convertData(data , dt, rowfst) ;
										}
									}
								}
							}
							if("col".equals(level.getLeveltype())){
								for(List<ValueData> vd : dataValueList){
									if(dt[0].equals("none")){
										vd.remove(dataindex);
									}else{
										if(dataindex<vd.size()){
											convertData(vd.get(dataindex) , dt, rowfst) ;
										}
									}
								}
							}
						}
					}else if(dt.length ==1){
						int rowindex = Integer.parseInt(rowfst[0].trim()) ;
						rowindex = rowindex>0 ? rowindex : 1 ;
						if(level.getTitle().size()>rowindex){
							if(dt[0].equals("replace")){
								List<Level> rowtitle = level.getTitle().get(rowindex) ;
								for(Level title : rowtitle){
									if(rowfst[2].equals(title.getNameValue())){
										title.setName(rowfst[3]) ;
									}
								}
							}
						}else{	//超出列范围，格式化数据
							int dataindex = rowindex - (reportData.getRow()!=null && reportData.getRow().getTitle()!=null ? level.getTitle().size() : 0);
							if(reportData.getRow()==null || "row".equals(level.getLeveltype()) && dataindex<dataValueList.size()){
								for(List<ValueData> vd : dataValueList){
									if(dataindex<vd.size()){
										convertData(vd.get(dataindex) , dt, rowfst) ;
									}
								}
							}
							if("col".equals(level.getLeveltype()) && dataindex<dataValueList.size()){
								List<ValueData> valueList = dataValueList.get(dataindex) ;
								for(ValueData data : valueList){
									convertData(data , dt, rowfst) ;
								}
							}

						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param data
	 * @param dt
	 * @param rowfst
	 */
	public void convertData(ValueData data , String[] dt , String[] rowfst){
		if(dt[0].equals("number") || rowfst[2].equals("number")){
			if(data.getValue()!=null && String.valueOf(data.getValue()).length()>0 && String.valueOf(data.getValue()).matches("[\\d]*?")){
				String value = new DecimalFormat(rowfst[2]).format(Float.parseFloat(data.getForamatValue()));
				data.setForamatValue(value);
			}
		}
		if(dt[0].equals("format") || rowfst[2].equals("format")){
			if(data.getValue()!=null && String.valueOf(data.getValue()).length()>0 && String.valueOf(data.getValue()).matches("[\\d]*?")){
				String value = new DecimalFormat(rowfst[3]).format(Float.parseFloat(data.getForamatValue()));
				data.setForamatValue(value);
			}
		}
		if(dt[0].equals("replace")){
			if(rowfst[2].equals(String.valueOf(data.getForamatValue()))){
				data.setForamatValue(rowfst[3]);
			}
		}
		if(rowfst[2].equals("default") || dt[0].equals("default")){
			if(data.getValue()==null || String.valueOf(data.getValue()).length()==0){
				data.setForamatValue(rowfst[3]);
			}
		}
	}
	private static Pattern pattern = Pattern.compile("[\\S\\s]*?<([\\S\\s]*?)>[\\s\\S]*?") ;
	/**
	 * 替换参数
	 * @param text
	 * @param request
	 * @return
	 */
	public String regexReplace(String text , HttpServletRequest request){
		Matcher matcher = pattern.matcher(text) ;
		StringBuffer strb = new StringBuffer() ;
		int index = 0 ;
		while(matcher.find()){
			if(matcher.groupCount()>=1){
				strb.append(text.substring(index, matcher.start())) ;
				index = matcher.end() ;
				String param = matcher.group(1) ;
				if(request.getParameter(param)!=null){
					strb.append(request.getParameter(param)) ;
				}
			}
		}
		if(index==0){
			strb.append(text) ;
		}else{
			strb.append(text.substring(index)) ;
		}
		return strb.toString() ;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return ServiceHelper.BusinessServiceEnum.REPORTDATAFACTORY.toString();
	}
	@Override
	public void service() throws Exception {
		
	}
}
