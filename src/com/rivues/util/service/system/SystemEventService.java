package com.rivues.util.service.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.rivues.core.RivuDataContext;
import com.rivues.util.service.monitor.BusinessService;

@Component
public class SystemEventService extends BusinessService{
	private static Logger log = LoggerFactory.getLogger(SystemEventService.class) ;
	private static boolean taskRuning = false ;
	@Scheduled(fixedRate = 1000*1)  
	public void service(){
		if (!taskRuning && RivuDataContext.isStart() && !RivuDataContext.isStopping()) {
			/**
			 * 系统日志处理，集群环境下，例如：一台服务器退出集群，其他所有服务器都会收到事件，同时都会往队列里写数据
			 */
		}
	}
	
	/**
	 * 服务名称
	 */
	public String getName(){
		return RivuDataContext.ServiceTypeName.SYSTEM_EVENT.toString();
	}
}
