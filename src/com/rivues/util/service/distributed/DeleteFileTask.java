package com.rivues.util.service.distributed;

import java.io.File;
import java.io.Serializable;
import java.util.concurrent.Callable;

import com.rivues.util.RivuTools;
import com.rivues.util.tools.RuntimeData;

public class DeleteFileTask implements Callable<String> , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String fileName ;
	private String orgi ;
	public DeleteFileTask(String fileName , String orgi){
		this.fileName = fileName ;
		this.orgi = orgi ;
	}
	@Override
	public String call() throws Exception {
		File exportFile = null ;
		if(this.fileName!=null){
			exportFile = RivuTools.getExportFileWithOutTimeOut(orgi, fileName) ;
			if(exportFile!=null && exportFile.exists()){
				exportFile.delete() ;
			}
			File exportJob = RivuTools.getExportJob(orgi, fileName) ;
			if(exportJob.exists()){
				exportJob.delete() ;
			}
		}
		return (exportFile!=null && RivuTools.getRunningJob(orgi, fileName) == null) ? new RuntimeData().getIpaddr():null;
	}
	

}
