package com.rivues.util.tools;

import java.text.ParseException;
import java.util.Date;

import org.quartz.CronExpression;

import com.rivues.util.RivuTools;


public class CronTools {
	/**
	 * 
	 * @param crontabExp
	 * @return
	 * @throws ParseException
	 */
	
	public static CronExpression getFireTime(String crontabExp) throws ParseException{
		return new CronExpression(crontabExp);
		
	}
	/**
	 * 
	 * @param crontabExp
	 * @return
	 * @throws ParseException
	 */
	
	public static Date getFinalFireTime(String crontabExp , Date date) throws ParseException{
		CronExpression expression = new CronExpression(crontabExp) ;
		return expression.getNextValidTimeAfter(date!=null ? date:new Date());
		
	}
	public static void main(String[] args) throws Exception{
		
		Date date = new Date();
		date.setHours(23);
		date.setMinutes(20);
		date.setSeconds(0);
		System.out.println(RivuTools.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
																				
		System.out.println(RivuTools.dateToString(CronTools.getFinalFireTime("0 0/20 12-23 * * ?",date), "yyyy-MM-dd HH:mm:ss"));
	}
}
