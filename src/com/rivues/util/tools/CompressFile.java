package com.rivues.util.tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipInputStream;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

import com.rivues.module.report.web.model.PublishedReport;
import com.rivues.util.serialize.JSON;

/**
 * 压缩文件
 * @author iceworld
 *
 */
public class CompressFile {
	/**
	 * 
	 * @param inputFile
	 * @param zipFilename
	 * @throws IOException
	 */
	public static void exportReport(List<PublishedReport> reportList , OutputStream output) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = null ;
        ZipOutputStream out = new ZipOutputStream(byteArrayOutputStream = new ByteArrayOutputStream());    
        try {    
        	for(PublishedReport report: reportList){
	        	out.putNextEntry(new ZipEntry(new StringBuffer().append(report.getName()).append(".rpt").toString()));   
	        	out.write(JSON.toJSONString(report).getBytes()) ;  
	        	out.close();
	        	out = new ZipOutputStream(byteArrayOutputStream);    
        	}
        } catch (IOException e) {    
            e.printStackTrace();  
        } finally {    
            out.close();    
            if(byteArrayOutputStream!=null){
            	byteArrayOutputStream.close() ;
            }
        }
        if(byteArrayOutputStream!=null){
        	output.write(byteArrayOutputStream.toByteArray()) ;
        }
	}
}
