package com.rivues.util.iface.cube;

import java.util.List;

import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.TypeCategory;
import com.rivues.module.platform.web.model.User;

public interface CubeInterface {
	
	public List<PublishedCube> getPublishedCubeList(String orgi , User user) ;
	
	public List<PublishedCube> getPublishedCubeListByDic(String orgi , User user , String dicid) ;
	
	public List<PublishedCube> getPublishedCubeListByDicAndType(String orgi , User user , String dicid , String dstype) ;
	
	public List<PublishedCube> getPublishedCubeListDistinctVerByDic(String orgi , User user , String dicid) ;
	
	public List<PublishedCube> getPublishedCubeListDistinctVerByDic(String orgi , User user , String tabid , String dicid , String type) ;
	
	public PublishedCube getCubeByID(String id) ;
	
	public Cube getCubeObjectByID(String id) ;
	
	public List<TypeCategory> getPublishedCubeDicList(String orgi ,User user) ;
	
	public List<TypeCategory> getPublishedCubeDicList(String orgi ,User user , String tabid) ;
	
	public List<TypeCategory> getTypeCategoryList(String orgi ,User user , String type , String tabid) ;
	
	public String getJobNameByCubeDic(String id,String orgi) ;
}
