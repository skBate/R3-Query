package com.rivues.util.iface.cube;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.TypeCategory;
import com.rivues.module.platform.web.model.User;

@SuppressWarnings("unchecked")
public class DatabaseCubeImpl implements CubeInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6609610372829684904L;

	@Override
	public List<PublishedCube> getPublishedCubeList(String orgi, User user) {
		return RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(PublishedCube.class).add(
						Restrictions.eq("orgi", orgi)));
	}

	@Override
	public PublishedCube getCubeByID(String id) {
		// TODO Auto-generated method stub
		List<PublishedCube> pcs = RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(PublishedCube.class).add(Restrictions.eq("id", id)));
		if(pcs.size()>0){
			return pcs.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public List<TypeCategory> getPublishedCubeDicList(String orgi, User user) {
		return getTypeCategoryList(orgi, user, RivuDataContext.TypeCategory.CUBE.toString() , RivuDataContext.TabType.PUB.toString()) ;
	}

	@Override
	public List<TypeCategory> getPublishedCubeDicList(String orgi, User user , String tabid) {
		return getTypeCategoryList(orgi, user, RivuDataContext.TypeCategory.CUBE.toString() , tabid) ;
	}

	@Override
	public List<PublishedCube> getPublishedCubeListByDic(String orgi,
			User user, String dicid) {
		return RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(PublishedCube.class).add(Restrictions.eq("typeid", dicid)).add(Restrictions.eq("orgi", orgi)));
	}

	@Override
	public List<TypeCategory> getTypeCategoryList(String orgi, User user,
			String type , String tabid) {
		return 
				RivuDataContext
				.getService().findAllByCriteria(DetachedCriteria.forClass(TypeCategory.class).add(Restrictions.eq("title", tabid)).add(Restrictions.eq("orgi", orgi)).add(Restrictions.eq("ctype", type))) ;
	}

	@Override
	public List<PublishedCube> getPublishedCubeListDistinctVerByDic(
			String orgi, User user, String dicid) {
		return getPublishedCubeListDistinctVerByDic(orgi, user, RivuDataContext.TabType.PUB.toString(), dicid , null) ;
		
	}
	
	@Override
	public List<PublishedCube> getPublishedCubeListDistinctVerByDic(
			String orgi, User user , String tabid , String dicid , String type) {
		DetachedCriteria criteria = 
				DetachedCriteria.forClass(PublishedCube.class).add(Restrictions.eq("db", tabid)).add(Restrictions.eq("typeid", dicid)).add(Restrictions.eq("orgi", orgi)).addOrder(Order.desc("dataversion")) ;
		if(type!=null){
			criteria.add(Restrictions.eq("dstype", type)) ;
		}
		List<PublishedCube> cubeList = RivuDataContext.getService().findAllByCriteria(criteria);
		Map<String , PublishedCube> valueMap = new HashMap<String , PublishedCube>();
		for(int i=0 ; i<cubeList.size() ;){
			PublishedCube cube = cubeList.get(i) ;
			if(valueMap.get(cube.getDataid())!=null){
				cubeList.remove(i) ;
				continue ;
			}
			valueMap.put(cube.getDataid(), cube);
			i++;
		}
		// TODO Auto-generated method stub
		return cubeList ;
		
	}

	@Override
	public Cube getCubeObjectByID(String id) {
		// TODO Auto-generated method stub
		List<Cube> pcs = RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(Cube.class).add(Restrictions.eq("id", id)));
		if(pcs.size()>0){
			return pcs.get(0);
		}else{
			return null;
		}
	}
	
	//根据模型类型选择模型  cube  table
	@Override
	public List<PublishedCube> getPublishedCubeListByDicAndType(String orgi,
			User user, String dicid, String dstype) {
		return RivuDataContext.getService().findAllByCriteria(
				DetachedCriteria.forClass(PublishedCube.class).add(Restrictions.eq("typeid", dicid)).add(Restrictions.eq("orgi", orgi)).add(Restrictions.eq("dstype",dstype)));
	}
	
	
	public String getJobNameByCubeDic(String id,String orgi){
		if("0".equals(id))return "";
		StringBuffer strb = new StringBuffer() ;
		List<TypeCategory> diclist = RivuDataContext.getService().findAllByCriteria(DetachedCriteria.forClass(TypeCategory.class).add(Restrictions.eq("orgi",orgi)));
		TypeCategory parent = null ;
		while(parent==null){
			for(TypeCategory type : diclist){
				if(type.getId().equals(id)){
					parent = type ;
					id = parent.getParentid();
					break ;
				}
			}
			if(parent!=null){
				strb.insert(0, parent.getName()).insert(0,"/");
				parent = null;
			}else{
				break ;
			}
		}
		return strb.toString();
	}

}
