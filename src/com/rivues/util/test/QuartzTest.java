package com.rivues.util.test;

import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.rivues.module.jobs.plugin.ReportJob;

public class QuartzTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SchedulerFactory schedulerFactory = new StdSchedulerFactory();
		try {
			final Scheduler scheduler = schedulerFactory.getScheduler();
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						Thread.sleep(3000) ;
						System.out.println("Startting......");
						JobDetail jobDetail = JobBuilder.newJob(ReportJob.class)
								.withIdentity("trigger1","group1").build();  
						Trigger trigger = TriggerBuilder.newTrigger()
								.startNow()
//								.withSchedule(CronScheduleBuilder.cronSchedule("0/2 * * * * ?"))  
						        .build();  
						scheduler.scheduleJob(jobDetail, trigger);  
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SchedulerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			thread.start();
			scheduler.start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
