package com.rivues.util.realm;

public class TrustedSSOAuthenticationToken implements org.apache.shiro.authc.HostAuthenticationToken, org.apache.shiro.authc.RememberMeAuthenticationToken{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4276449192523852620L;
	private String host ;
	private String username ;
	private String password ;
	private String orgi ;
	private boolean rememberMe ;
	
	public TrustedSSOAuthenticationToken(){}
	public TrustedSSOAuthenticationToken(String username , String password, String orgi){
		this.username = username ;
		this.password = password ;
		this.orgi = orgi ;
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	@Override
	public Object getCredentials() {
		// TODO Auto-generated method stub
		return this.username;
	}

	@Override
	public Object getPrincipal() {
		return this.username;
	}
	public void setRememberMe(boolean rememberMe){
		this.rememberMe = rememberMe ;
	}
	@Override
	public boolean isRememberMe() {
		// TODO Auto-generated method stub
		return this.rememberMe ;
	}
	

	

}
