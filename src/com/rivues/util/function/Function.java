package com.rivues.util.function;

public interface Function {
	public Object excute(String functionstr);
}
