package com.rivues.util.exception;

public class DicExistEception extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5509859751622030574L;
	/**
	 * 创建 错误已存在异常
	 * @param error
	 */
	public DicExistEception(String error){
		super(error) ;
	}

}
