package com.rivues.util.exception;

public class DicNotExistException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5509859751622030574L;
	/**
	 * 创建 错误已存在异常
	 * @param error
	 */
	public DicNotExistException(String error){
		super(error) ;
	}

}
