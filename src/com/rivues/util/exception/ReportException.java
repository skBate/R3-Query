package com.rivues.util.exception;

public class ReportException extends RivuException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4064341279656002263L;

	public ReportException(String msg){
		super(msg) ;
	}

}
