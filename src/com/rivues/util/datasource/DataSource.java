package com.rivues.util.datasource;

import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.rivues.module.platform.web.model.AnalyzerReportModel;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.QueryLog;
import com.rivues.module.platform.web.model.QueryText;
import com.rivues.module.platform.web.model.ReportFilter;
import com.rivues.module.platform.web.model.User;
import com.rivues.util.data.ReportData;
import com.rivues.util.tools.ExportFile;

public interface DataSource {
	/**
	 * 数据查询功能
	 * @param model
	 * @param requestParamValues
	 * @param cube
	 * @param request
	 * @param parseParam
	 * @param queryText
	 * @param queryLog
	 * @return
	 * @throws Exception
	 */
	public ReportData getData(AnalyzerReportModel model , Map<String , Object> requestParamValues , Cube cube ,HttpServletRequest request , boolean parseParam , QueryText queryText , QueryLog queryLog) throws Exception;
	
	/**
	 * 导出数据功能
	 * @param model
	 * @param requestParamValues
	 * @param cube
	 * @param request
	 * @param parseParam
	 * @param queryText
	 * @param export
	 * @throws Exception
	 */
	public void getRSData(AnalyzerReportModel model , Map<String , Object> requestParamValues , Cube cube ,HttpServletRequest request , boolean parseParam , QueryText queryText , ExportFile export) throws Exception;
	
	/**
	 * 获取虚拟数据功能
	 * @param model
	 * @param requestParamValues
	 * @param cube
	 * @param request
	 * @param parseParam
	 * @param queryText
	 * @param export
	 * @throws Exception
	 */
	public ReportData getVData(AnalyzerReportModel model , Map<String , Object> requestParamValues , Cube cube ,HttpServletRequest request , boolean parseParam , QueryText queryText , QueryLog queryLog) throws Exception;
	
	
	public void refresh(String ds , Cube cube) ;
	public QueryText getQuery(AnalyzerReportModel model, Cube cube , HttpServletRequest request , boolean parseParam , QueryLog queryLog) ;
	public QueryText getQuery(AnalyzerReportModel model, Cube cube , HttpServletRequest request  , com.rivues.module.platform.web.model.CubeLevel level , String con , QueryLog queryLog, boolean useStaticFilters, ReportFilter filter) ;
	
	public Date getDate() ;
	public String processParam(String value , Cube cube , AnalyzerReportModel model , HttpServletRequest request , boolean parseParam , QueryText queryText) ;
	
	public boolean valid(Cube cube) throws Exception ;
}