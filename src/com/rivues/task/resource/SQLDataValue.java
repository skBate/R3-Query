package com.rivues.task.resource;

import java.util.ArrayList;
import java.util.List;

public class SQLDataValue {
	private String sql ;
	private List<String> metadata = new ArrayList<String>();
	private int paranum = 0;
	private String id ;
	public SQLDataValue(){}
	public SQLDataValue(String sql , List<String> meta){
		this.metadata = meta ;
	}
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public List<String> getMetadata() {
		return metadata;
	}
	
	public int getParanum() {
		return paranum;
	}
	public void setParanum(int paranum) {
		this.paranum = paranum;
	}
	public void setMetadata(List<String> metadata) {
		this.metadata = metadata;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
