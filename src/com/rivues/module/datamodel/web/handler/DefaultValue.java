package com.rivues.module.datamodel.web.handler;

import com.rivues.util.RivuTools;

public class DefaultValue implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3619003858393888176L;
	
	
	private String name ;
	private String value ;
	private String format ;
	
	public DefaultValue(String name , String value , String format){
		this.name = name ;
		this.value = value ;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		if(this.name.matches("[ ]{0,}[Tt]{1,}[ ]{0,}[+-]{0,1}([\\d]{0,})")){
			this.value = RivuTools.getDays(name , format);
		}
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	
}
