package com.rivues.module.platform.web.exception;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.ExceptionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.rivues.module.platform.web.handler.Handler;
import com.rivues.util.RivuTools;
import com.rivues.util.log.Logger;
import com.rivues.util.log.LoggerFactory;

/**
 * 异常处理类
 * @author outofmemory.cn
 *
 */
public class ExceptionResolver extends SimpleMappingExceptionResolver implements HandlerExceptionResolver {
	private final Logger log = LoggerFactory.getLogger(ExceptionResolver.class);   
	/**
	 * 根据 ex 类型来定义不同的错误提示消息， 每个地方需要 抛出异常的 异常代码
	 */
    @Override
    public ModelAndView resolveException(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex) {
        HttpSession session = request.getSession();
        if (session != null) {
        	/**
        	 * 异常信息记录
        	 */
        }
        ModelAndView view = super.getModelAndView("/error/exception" , ex);
        HandlerMethod  hander = (HandlerMethod ) handler ;
        Handler bean = (Handler)hander.getBean() ;
        bean.setError(true) ;
	    log.error(ex , bean.getOrgi() ,  bean.getFlowid()) ;
	    view.addObject("date", RivuTools.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        view.addObject("ex", ex) ;
        view.addObject("message", ExceptionUtils.getFullStackTrace(ex)) ;
        return view ;
    }

}