package com.rivues.module.platform.web.auth;

import java.util.List;
import java.util.Map;




import com.rivues.module.platform.web.model.Auth;
import com.rivues.module.platform.web.model.DataDic;
import com.rivues.module.platform.web.model.User;
import com.rivues.module.report.web.model.PublishedReport;
/**
 * 权限接口
 * @author huqi
 * @date 2014/5/11
 *
 */
public interface AuthInterface {
	/**
	 * 增加权限拥有者列表
	 * @param reportid
	 * @param ids
	 * @param orgi
	 * @param ownerType
	 * @throws Exception
	 */
	public void addOwnerList(String resourceid,String ids,String orgi,String ownerType,String isDic) throws Exception;
	
	/**
	 * 更新资源权限
	 * @param resourceid
	 * @param list
	 * @throws Exception
	 */
	public void updateAuth(String resourceid,List<Auth> list)  throws Exception;
	/**
	 * 获取报表的授权列表
	 * @param reportid
	 * @param orgi
	 * @return
	 */
	public List<Auth> getReportAuthList(String reportid,String orgi);
	/**
	 * 过滤报表权限
	 * @param report
	 * @param user
	 */
	public void filterReportAuth(PublishedReport report,User user);
	
	/**
	 * 过滤目录权限
	 * @param dic
	 * @param user
	 */
	public List<Auth> filterDicAuth(User user ,String orgi , String authType);
	/**
	 * 获取目录的授权信息
	 * @param dicid
	 * @param orgi
	 * @return
	 */
	public List<Auth> getDicAuthList(String dicid,String orgi);
	
	/**
	 * 过滤目录权限
	 * @param dic
	 * @param user
	 */
	public List<Auth> filterAuth(DataDic dic,User user ,List<DataDic> dicList ,  String orgi , String authType);
	
	/**
	 * 根据条件获取权限信息
	 */
	public List<Auth> getAuthlist(String resourceid,String resourcetype,String ownerid,String ownertype);
	
	/**
	 * 获取用户自己所有的权限，包括他的父级的权限（角色权限和机构权限）
	 * 注意：用户优先级>角色优先级>机构优先级，
	 * @param userid
	 * @return map的key值为resourceid_resourcetype类型；
	 */
	public Map<String, Auth> getUserSelfAuthlist(String userid);
	
	/**
	 * 检查权限是否存在
	 * @param resourceid
	 * @param map
	 * @return
	 */
	public boolean checkIsAuth(String resourceid ,String resourcetype,String userid);
	
	/**
	 * 检查权限是否存在
	 * @param resourceid
	 * @param map
	 * @return
	 */
	public boolean checkIsAuth(String resourceid ,String resourcetype,String userid,List<Auth> auths);
}
