package com.rivues.module.platform.web.model;

public class Warning implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2445824016886069354L;
	private String id ;
	private String name ;
	private String dataname ;
	private String valuetype;
	private String stylevalue ;		
	private String measure ;		//指标列
	private String convalue ;		//条件符合后处理类型，字体颜色，显示图标，背景颜色，字体样式，替换文本
	private String comparemeasure ;	//预警比较的 目标指标
	private String comparevalue ;	//预警比较的 目标值
	private String iscomparevalue; //是否值比较 0：按指标比较，1：按值比较
	private String condition ;		//比较类型条件 条件类型  == , != , > , >= , < , <=
	private String align;          //图表位置，left,right,replace
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDataname() {
		return dataname;
	}
	public void setDataname(String dataname) {
		this.dataname = dataname;
	}
	public String getValuetype() {
		return valuetype;
	}
	public void setValuetype(String valuetype) {
		this.valuetype = valuetype;
	}
	public String getStylevalue() {
		return stylevalue;
	}
	public void setStylevalue(String stylevalue) {
		this.stylevalue = stylevalue;
	}
	public String getMeasure() {
		return measure;
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
	public String getConvalue() {
		return convalue;
	}
	public void setConvalue(String convalue) {
		this.convalue = convalue;
	}
	public String getComparemeasure() {
		return comparemeasure;
	}
	public void setComparemeasure(String comparemeasure) {
		this.comparemeasure = comparemeasure;
	}
	public String getComparevalue() {
		return comparevalue;
	}
	public void setComparevalue(String comparevalue) {
		this.comparevalue = comparevalue;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getIscomparevalue() {
		return iscomparevalue;
	}
	public void setIscomparevalue(String iscomparevalue) {
		this.iscomparevalue = iscomparevalue;
	}
	public String getAlign() {
		return align;
	}
	public void setAlign(String align) {
		this.align = align;
	}
}
