package com.rivues.module.platform.web.model;

import org.apache.commons.lang.StringUtils;

public class StyleBean implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 518473692444290526L;
	private String id ;
	private String dataid ;
	private String type ;
	private String color;
	private String backgroundcolor;
	private String font_size ;
	private String font_family ;
	private String font_weight ;
	private String font_style ;
	private String text_decoration ;
	private String margin ;
	private String textalign ;
	private String oddcolor ;
	private String evencolor ;
	private String pline ;	//奇偶行
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getFont_size() {
		return font_size;
	}
	public void setFont_size(String font_size) {
		this.font_size = font_size;
	}
	public String getFont_family() {
		return font_family;
	}
	public void setFont_family(String font_family) {
		this.font_family = font_family;
	}
	public String getFont_weight() {
		return font_weight;
	}
	public void setFont_weight(String font_weight) {
		this.font_weight = font_weight;
	}
	
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getFont_style() {
		return font_style;
	}
	public void setFont_style(String font_style) {
		this.font_style = font_style;
	}
	public String getText_decoration() {
		return text_decoration;
	}
	public void setText_decoration(String text_decoration) {
		this.text_decoration = text_decoration;
	}
	public String getTextalign() {
		return textalign;
	}
	public void setTextalign(String textalign) {
		this.textalign = textalign;
	}
	public String getOddcolor() {
		return oddcolor;
	}
	public void setOddcolor(String oddcolor) {
		this.oddcolor = oddcolor;
	}
	public String getEvencolor() {
		return evencolor;
	}
	public void setEvencolor(String evencolor) {
		this.evencolor = evencolor;
	}
	public String getBackgroundcolor() {
		return backgroundcolor;
	}
	public void setBackgroundcolor(String backgroundcolor) {
		this.backgroundcolor = backgroundcolor;
	}
	public String getPline() {
		return pline;
	}
	public void setPline(String pline) {
		this.pline = pline;
	}
	/**
	 * 
	 * @return
	 */
	public String getStyle(){
		StringBuffer strb = new StringBuffer();
		if(!StringUtils.isEmpty(color)){
			strb.append("color:").append(color).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(font_size)){
			strb.append("font-size:").append(font_size).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(font_family)){
			strb.append("font-family:").append(font_family).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(font_weight)){
			strb.append("font-weight:").append(font_weight).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(font_style)){
			strb.append("font-style:").append(font_style).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(text_decoration)){
			strb.append("text-decoration:").append(text_decoration).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(margin)){
			strb.append("margin:").append(margin).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(textalign)){
			strb.append("text-align:").append(textalign).append(" !important;") ;
		}
		if(!StringUtils.isEmpty(backgroundcolor)){
			strb.append("background-color:").append(backgroundcolor).append(" !important;") ;
		}
		return strb.toString() ;
	}
}
