/**
 * Licensed to the Rivulet under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     webapps/LICENSE-Rivulet-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rivues.module.platform.web.model;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.rivues.core.RivuDataContext;

/**
 * @author jaddy0302 Rivulet JobDetail.java 2010-3-1
 * 
 */
@Entity
@Table(name = "rivu5_jobdetail")
@org.hibernate.annotations.Proxy(lazy = false)
public class JobDetail implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3038981315537092556L;
	private String id ;
	private String name ;
	private String clazz ;			//结算表的时候，危险操作提示
	private String taskid ;	 //任务目标id
	private String tasktype ; //table 模型任务 、report报表计划任务
	private boolean plantask;    // true后台任务，false交互式任务
	private String source ;   //File source eg:e:\document
	private String userid;		//task user
	private String username;	//task user name
	private String email;
	private String nickname;
	private String crawltaskid ;//用来标记是手动点击的还是到了执行时间执行的，手动：operation，自动：auto或null
	private long lastindex = 0 ;
	private int pages = 0 ;
	private String dataid;//为了在状态中检索，存放了报表的id集合
	private String dicid;//存放了报表所在目录
	
	@Transient
	private int fetchNum ;
	@Transient
	private int docNum ;

	private Date taskfiretime ;
	private String crawltask ;
	private String targettask ;
	private boolean createtable;
	private String taskstatus = "0";
	private long startindex ;	//数据更新位置
	private Date lastdate ;		//数据更新时间
	private Date nextfiretime ;		//数据更新时间
	private String cronexp ;
	private boolean fetcher = true ;
	private boolean pause = false;
	private boolean plantaskreadtorun = false ;
	@Transient
	private Reporter report ;
	@Transient
	private boolean metadata = false;
	
	@Transient
	private String ExceptionMsg = null;

	private String orgi ;	
	private String memo ;		//任务类型，如果是 cubedata , 则在执行完毕后更新cube信息   , 导出任务时改变用处，用于导出是的 导出格式
	private long fetchSize ;
	private String usearea ;	//启用分区采集    ， 改变用处，在 导出文件的时候，用于记录当前导出的 数据集的 total
	private String areafield ;	//分区字段			
	private String areafieldtype ;	//字段类型
	private String arearule ;	//分区递增规则
	private String minareavalue ;	//最小值
	private String maxareavalue ;	//最大值
	private String formatstr ;		//格式化字符串
	private String taskinfo;       //序列化以后的结果，JSON格式
	private int priority;           //优先级
	private String runserver ;		//执行此任务的 服务器

	
	public String getTaskinfo() {
		return taskinfo;
	}
	public void setTaskinfo(String taskinfo) {
		this.taskinfo = taskinfo;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	@Transient
	private FileOutputStream out = null;
	
	@Transient
	public void initCsvData(String taskName) throws FileNotFoundException{
		File csvFile = new File(RivuDataContext.REAL_PATH + File.separator + "data/csv", taskName);
		out = new FileOutputStream(csvFile , true) ;
	}
	@Transient
	public synchronized void writeCsvData(String text) throws UnsupportedEncodingException, IOException{
		if(out!=null && text!=null && text.length()>0){
			out.write(text.getBytes("UTF-8")) ;
			out.write("\n".getBytes());
		}
	}
	@Transient
	public void closeCsvData() throws IOException{
		if(out!=null){
			out.close();
		}
	}
	
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the clazz
	 */
	public String getClazz() {
		return clazz;
	}
	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	/**
	 * @return the taskid
	 */
	public String getTaskid() {
		return taskid;
	}
	/**
	 * @param taskid the taskid to set
	 */
	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}
	/**
	 * @return the tasktype
	 */
	public String getTasktype() {
		return tasktype;
	}
	/**
	 * @param tasktype the tasktype to set
	 */
	public void setTasktype(String tasktype) {
		this.tasktype = tasktype;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the taskfiretime
	 */
	public Date getTaskfiretime() {
		return taskfiretime;
	}
	/**
	 * @param taskfiretime the taskfiretime to set
	 */
	public void setTaskfiretime(Date taskfiretime) {
		this.taskfiretime = taskfiretime;
	}
	/**
	 * @return the report
	 */
	@Transient
	public Reporter getReport() {
		return report;
	}
	/**
	 * @param report the report to set
	 */
	public void setReport(Reporter report) {
		this.report = report;
	}

	
	public void setStartindex(long startindex) {
		this.startindex = startindex;
	}
	public Date getLastdate() {
		return lastdate;
	}
	public void setLastdate(Date lastdate) {
		this.lastdate = lastdate;
	}
	
	/**
	 * @return the crawltaskid
	 */
	public String getCrawltaskid() {
		return crawltaskid;
	}
	/**
	 * @param crawltaskid the crawltaskid to set
	 */
	public void setCrawltaskid(String crawltaskid) {
		this.crawltaskid = crawltaskid;
	}
	
	
	/**
	 * @return the fetcher
	 */
	public boolean isFetcher() {
		return fetcher;
	}
	/**
	 * @param fetcher the fetcher to set
	 */
	public void setFetcher(boolean fetcher) {
		this.fetcher = fetcher;
	}
	/**
	 * @return the pause
	 */
	public boolean isPause() {
		return pause;
	}
	/**
	 * @param pause the pause to set
	 */
	public void setPause(boolean pause) {
		this.pause = pause;
	}
	/**
	 * @return the plantask
	 */
	public boolean isPlantask() {
		return plantask;
	}
	/**
	 * @param plantask the plantask to set
	 */
	public void setPlantask(boolean plantask) {
		this.plantask = plantask;
	}
	/**
	 * @return the plantaskreadtorun
	 */
	@Transient
	public boolean isPlantaskreadtorun() {
		return plantaskreadtorun;
	}
	/**
	 * @param plantaskreadtorun the plantaskreadtorun to set
	 */
	public void setPlantaskreadtorun(boolean plantaskreadtorun) {
		this.plantaskreadtorun = plantaskreadtorun;
	}
	@Transient
	public int getFetchNum() {
		return fetchNum;
	}
	public void setFetchNum(int fetchNum) {
		this.fetchNum = fetchNum;
	}
	@Transient
	public int getDocNum() {
		return docNum;
	}
	public void setDocNum(int docNum) {
		this.docNum = docNum;
	}
	@Transient
	public boolean isMetadata() {
		return metadata;
	}
	public void setMetadata(boolean metadata) {
		this.metadata = metadata;
	}
	public boolean isCreatetable() {
		return createtable;
	}
	public void setCreatetable(boolean createtable) {
		this.createtable = createtable;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Date getNextfiretime() {
		return nextfiretime;
	}
	public void setNextfiretime(Date nextfiretime) {
		this.nextfiretime = nextfiretime;
	}
	public String getCronexp() {
		return cronexp;
	}
	public void setCronexp(String cronexp) {
		this.cronexp = cronexp;
	}
	public String getTaskstatus() {
		return taskstatus;
	}
	public void setTaskstatus(String taskstatus) {
		this.taskstatus = taskstatus;
	}
	@Transient
	public long getFetchSize(){
		return this.clazz!=null && this.clazz.matches("[\\d]{1,}") ? Long.parseLong(this.clazz) : 50000 ;
	}
	@Transient
	public void setFetchSize(long fetchSize) {
		this.fetchSize = fetchSize;
	}
	@Transient
	public long getLastindex() {
		return lastindex;
	}
	@Transient
	public void setLastindex(long lastindex) {
		this.lastindex = lastindex;
	}
	public String getUsearea() {
		return usearea;
	}
	public void setUsearea(String usearea) {
		this.usearea = usearea;
	}
	public String getAreafield() {
		return areafield;
	}
	public void setAreafield(String areafield) {
		this.areafield = areafield;
	}
	public String getAreafieldtype() {
		return areafieldtype;
	}
	public void setAreafieldtype(String areafieldtype) {
		this.areafieldtype = areafieldtype;
	}
	public String getArearule() {
		return arearule;
	}
	public void setArearule(String arearule) {
		this.arearule = arearule;
	}
	public String getMinareavalue() {
		return minareavalue;
	}
	public void setMinareavalue(String minareavalue) {
		this.minareavalue = minareavalue;
	}
	public String getMaxareavalue() {
		return maxareavalue;
	}
	public void setMaxareavalue(String maxareavalue) {
		this.maxareavalue = maxareavalue;
	}
	public String getFormatstr() {
		return formatstr;
	}
	public void setFormatstr(String formatstr) {
		this.formatstr = formatstr;
	}
	@Transient
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getCrawltask() {
		return crawltask;
	}
	public void setCrawltask(String crawltask) {
		this.crawltask = crawltask;
	}
	public String getTargettask() {
		return targettask;
	}
	public void setTargettask(String targettask) {
		this.targettask = targettask;
	}

	public long getStartindex() {
		return startindex;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getDicid() {
		return dicid;
	}
	public void setDicid(String dicid) {
		this.dicid = dicid;
	}
	@Transient
	public Date getStarttime(){
		return this.getNextfiretime() ;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	@Transient
	public String getRunserver() {
		return runserver;
	}
	public void setRunserver(String runserver) {
		this.runserver = runserver;
	}
	@Transient
	public String getExceptionMsg() {
		return ExceptionMsg;
	}
	public void setExceptionMsg(String exceptionMsg) {
		ExceptionMsg = exceptionMsg;
	}
}
