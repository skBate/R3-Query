package com.rivues.module.platform.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.rivues.module.report.web.model.ModelPackage;
import com.rivues.util.RivuTools;
import com.rivues.util.data.ReportData;
import com.rivues.util.serialize.JSON;
import com.rivues.util.service.cache.CacheHelper;
import com.rivues.util.tools.ExportFile;

@Entity
@Table(name = "rivu5_analyzereportmodel")
@org.hibernate.annotations.Proxy(lazy = false)
public class AnalyzerReportModel implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id ;
	private String posx ;
	private String posy ;
	private String poswidth;
	private String posheight;
	private String name ;
	private String code ;
	private String reportid ;
	private String modeltype ;
	private int sortindex ;
	private String stylestr;
	private String labeltext ;
	private String cssclassname ;
	private String mposleft;	//指标位置
	private String mpostop ;	//指标位置
	private String title ;
	private boolean exchangerw = false ;	//行列转换
	private String publishedcubeid ;
	private String rowdimension ;	//逗号分隔
	private String coldimension ;	//逗号分隔
	private String measure ;		//逗号分隔
	private String dstype ;		//数据源类型cube or table
	private String dbType;      //数据库类型：sqlserver oracle mysql hive ....
	private String orgi ;
	private String objectid ;
	private Date createtime ;
	private String filterstr;
	private String sortstr ;
	private String viewtype = "view";	//视图类型, 图表,组件, 仪表盘
	private String chartemplet ;		//图表显示数据XML文件的模板,存放显示模板的id
	private String chartype ;			//图表类型
	private String chartdatatype	;	//图表数据类型
	private String chart3d = "false";			//显示为3D图表
	private String xtitle ;				//修改用处，对图表，用于控制是否显示X轴标题 ， 对于表格，用于控制需要隐藏的参数
	private String ytitle ;				//修改用处。对图表，用于控制是否显示Y轴标题，对于表格，用于控制当前当前选中的参数的名称
	private String charttitle = "false";
	private String displayborder ;			//显示边框
	private String bordercolor = "#FFFFFF";			//边框颜色
	private String displaydesc ;	//显示颜色说明区域
	private String formdisplay ;	//表单内显示模式 ，行内（Form-Inline）：换行（form-horizontal）
	private String labelstyle ;		//表单样式
	private String formname ;		//表单名称 name
	private String defaultvalue ;	//默认值
	private String querytext ;		//查询语法
	private String tempquey ;		//保存当次查询语句
	private boolean displaytitle = false ;	//显示表头
	private boolean clearzero = false ;	//除零
	
	private String titlestr ;
	private String width = "100";
	private String height = "100";
	private String widthunit = "%";	//宽度单位  % OR px
	private String heightunit = "%"; 	//高度单位 % OR px
	private String defheight = "200px";   //默认内部元素的高度，默认为 200px
	private String defwidth = "98%" ;
	
	private String neckwidth ;
	private String neckheight ;
	private String extparam ;
	private String marginright ;
	
	private String colorstr ; 
	
	private String start ;
	private String end ;
	private String rowformatstr ;
	private String colformatstr ;
	private String publishtype ;		//发布类型
	private List<DrillDown> drilldown = new ArrayList<DrillDown>();
	private List<Warning> warning = new ArrayList<Warning>();
	
	private List<ColumnProperties> properties = new ArrayList<ColumnProperties>();
	private String editview ="view";			//编辑视图
	
	private List<ReportFilter> filters  ;
	private StyleBean style ;
	private StyleBean oddstyle ;
	private StyleBean evenstyle ;
	private boolean expandbtm = true;	//展开底部工具栏
	private boolean expandrgt = true;	//展开右侧工具栏
	private String curtab 	;	 //当前默认展开的 工具栏  ： 数据 ： 组件
	
	private String hiddencolstr;//隐藏的列
	private String micrometercolstr;//千分位显示的列

	private String eventstr ;	//事件
	private String dsmodel;	//数据源对象
	private ReportData reportData ; 
	
	private String sqldialect;
	private int pageSize = 20;
	private String isloadfulldata = "false";
	private boolean isexport;
	private AnalyzerReportPageSetting pagesetting;//页面设置
	
	private List<MeasureGroup> groupList = new ArrayList<MeasureGroup>();
	private List<ModelPackage> modelPackageList = new ArrayList<ModelPackage>();	//DashBoard 和动态报表的 页面内容
	
	private ExportFile export ;
	
	private String exporttitle ;
	
	private int colSize = 100;//报表显示列大小
	
	private String sortType;//排序类型
	private String sortName;//排序列名称；
	
	@Transient
	public List<MeasureGroup> getGroupList() {
		return groupList == null ? groupList = new ArrayList<MeasureGroup>() : groupList;
	}
	public void setGroupList(List<MeasureGroup> groupList) {
		this.groupList = groupList;
	}
	private Cube cube ;
	
	
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Transient
	public List<DrillDown> getDrilldown() {
		return drilldown;
	}
	public void setDrilldown(List<DrillDown> drilldown) {
		this.drilldown = drilldown;
	}
	@Transient
	public List<ColumnProperties> getProperties() {
		return properties;
	}
	public void setProperties(List<ColumnProperties> properties) {
		this.properties = properties;
	}
	@Transient
	public List<ReportFilter> getFilters() {
		return filters;
	}
	
	public void setFilters(List<ReportFilter> filters) {
		this.filters = filters;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getModeltype() {
		return modeltype;
	}
	public void setModeltype(String modeltype) {
		this.modeltype = modeltype;
	}
	public String getStylestr() {
		return stylestr;
	}
	public void setStylestr(String stylestr) {
		this.stylestr = stylestr;
	}
	public String getCssclassname() {
		return cssclassname!=null && cssclassname.length()>0?cssclassname:"true";
	}
	public void setCssclassname(String cssclassname) {
		this.cssclassname = cssclassname;
	}
	public String getMposleft() {
		return mposleft;
	}
	public void setMposleft(String mposleft) {
		this.mposleft = mposleft;
	}
	public String getMpostop() {
		return mpostop;
	}
	public void setMpostop(String mpostop) {
		this.mpostop = mpostop;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDstype() {
		return dstype;
	}
	public void setDstype(String dstype) {
		this.dstype = dstype;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	
	public String getObjectid() {
		return objectid;
	}
	public void setObjectid(String objectid) {
		this.objectid = objectid;
	}
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public int getSortindex() {
		return sortindex;
	}
	public void setSortindex(int sortindex) {
		this.sortindex = sortindex;
	}
	public String getRowdimension() {
		return rowdimension!=null ? rowdimension:"";
	}
	public void setRowdimension(String rowdimension) {
		this.rowdimension = rowdimension;
	}
	public String getColdimension() {
		return coldimension!=null ? coldimension : "";
	}
	public void setColdimension(String coldimension) {
		this.coldimension = coldimension;
	}
	public String getMeasure() {
		return measure !=null ? measure : "";
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
	@Transient
	public ReportData getReportData() {
		return reportData;
	}
	public void setReportData(ReportData reportData) {
		this.reportData = reportData;
	}
	public String getDsmodel() {
		return dsmodel;
	}
	public void setDsmodel(String dsmodel) {
		this.dsmodel = dsmodel;
	}
	public String getEventstr() {
		return eventstr;
	}
	public void setEventstr(String eventstr) {
		this.eventstr = eventstr;
	}
	public String getViewtype() {
		return viewtype;
	}
	public void setViewtype(String viewtype) {
		this.viewtype = viewtype;
	}
	public String getChartemplet() {
		return chartemplet;
	}
	public void setChartemplet(String chartemplet) {
		this.chartemplet = chartemplet;
	}
	public String getChartdatatype() {
		return chartdatatype;
	}
	public void setChartdatatype(String chartdatatype) {
		this.chartdatatype = chartdatatype;
	}
	public String getChart3d() {
		return chart3d;
	}
	public void setChart3d(String chart3d) {
		this.chart3d = chart3d;
	}
	public String getXtitle() {
		return xtitle;
	}
	public void setXtitle(String xtitle) {
		this.xtitle = xtitle;
	}
	public String getYtitle() {
		return ytitle;
	}
	public void setYtitle(String ytitle) {
		this.ytitle = ytitle;
	}
	public String getCharttitle() {
		return charttitle;
	}
	public void setCharttitle(String charttitle) {
		this.charttitle = charttitle;
	}
	public String getDisplayborder() {
		return displayborder;
	}
	public void setDisplayborder(String displayborder) {
		this.displayborder = displayborder;
	}
	public String getBordercolor() {
		return bordercolor;
	}
	public void setBordercolor(String bordercolor) {
		this.bordercolor = bordercolor;
	}
	public String getDisplaydesc() {
		return displaydesc;
	}
	public void setDisplaydesc(String displaydesc) {
		this.displaydesc = displaydesc;
	}
	public String getFilterstr() {
		return filterstr;
	}
	public void setFilterstr(String filterstr) {
		this.filterstr = filterstr;
	}
	public String getSortstr() {
		return sortstr;
	}
	public void setSortstr(String sortstr) {
		this.sortstr = sortstr;
	}
	public String getLabeltext() {
		return labeltext!=null && labeltext.length()>0 ? labeltext:null;
	}
	public void setLabeltext(String labeltext) {
		this.labeltext = labeltext;
	}
	public String getFormdisplay() {
		return formdisplay;
	}
	public void setFormdisplay(String formdisplay) {
		this.formdisplay = formdisplay;
	}
	public String getLabelstyle() {
		return labelstyle;
	}
	public void setLabelstyle(String labelstyle) {
		this.labelstyle = labelstyle;
	}
	public String getFormname() {
		return formname;
	}
	public void setFormname(String formname) {
		this.formname = formname;
	}
	public String getDefaultvalue() {
		return defaultvalue!=null && defaultvalue.length()>0 ? defaultvalue: null;
	}
	public void setDefaultvalue(String defaultvalue) {
		this.defaultvalue = defaultvalue;
	}
	public boolean isExchangerw() {
		return exchangerw;
	}
	public void setExchangerw(boolean exchangerw) {
		this.exchangerw = exchangerw;
	}
	public String getQuerytext() {
		return querytext;
	}
	public void setQuerytext(String querytext) {
		this.querytext = querytext;
	}
	@Transient
	public String getTempquey() {
		return tempquey;
	}
	public void setTempquey(String tempquey) {
		this.tempquey = tempquey;
	}
	public boolean isDisplaytitle() {
		return displaytitle;
	}
	public void setDisplaytitle(boolean displaytitle) {
		this.displaytitle = displaytitle;
	}
	public String getTitlestr() {
		return titlestr;
	}
	public void setTitlestr(String titlestr) {
		this.titlestr = titlestr;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name = "startr")
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	@Column(name = "endt")
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getRowformatstr() {
		return rowformatstr;
	}
	public void setRowformatstr(String rowformatstr) {
		this.rowformatstr = rowformatstr;
	}
	public String getColformatstr() {
		return colformatstr;
	}
	public void setColformatstr(String colformatstr) {
		this.colformatstr = colformatstr;
	}
	@Transient
	public String getPublishtype() {
		return publishtype;
	}
	public void setPublishtype(String publishtype) {
		this.publishtype = publishtype;
	}
	@Transient
	public String getEditview() {
		return editview;
	}
	public void setEditview(String editview) {
		this.editview = editview;
	}
	public String getSqldialect() {
		return sqldialect;
	}
	public void setSqldialect(String sqldialect) {
		this.sqldialect = sqldialect;
	}
	public int getPageSize() {
		return pageSize >0 ? pageSize : 20;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getIsloadfulldata() {
		return isloadfulldata;
	}
	public void setIsloadfulldata(String isloadfulldata) {
		this.isloadfulldata = isloadfulldata;
	}
	@Transient
	public boolean isIsexport() {
		return isexport;
	}
	public void setIsexport(boolean isexport) {
		this.isexport = isexport;
	}
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="pagesetting")
	public AnalyzerReportPageSetting getPagesetting() {
		return pagesetting;
	}
	public void setPagesetting(AnalyzerReportPageSetting pagesetting) {
		this.pagesetting = pagesetting;
	}
	public String getPublishedcubeid() {
		return publishedcubeid;
	}
	public void setPublishedcubeid(String publishedcubeid) {
		this.publishedcubeid = publishedcubeid;
	}
	/**
	 * 获取 Model 的样式列表，包括整个报表和  报表的 列  和行
	 * @return
	 */
	@Transient
	public List<StyleBean> getStyles(){
		return !StringUtils.isEmpty(stylestr) ? JSON.parseArray(this.stylestr, StyleBean.class) : null ;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getNeckwidth() {
		return neckwidth;
	}
	public void setNeckwidth(String neckwidth) {
		this.neckwidth = neckwidth;
	}
	public String getNeckheight() {
		return neckheight;
	}
	public void setNeckheight(String neckheight) {
		this.neckheight = neckheight;
	}
	public String getExtparam() {
		return extparam;
	}
	public void setExtparam(String extparam) {
		this.extparam = extparam;
	}
	public String getColorstr() {
		return colorstr;
	}
	public void setColorstr(String colorstr) {
		this.colorstr = colorstr;
	}
	public String getMarginright() {
		return marginright;
	}
	public void setMarginright(String marginright) {
		this.marginright = marginright;
	}
	public boolean isClearzero() {
		return clearzero;
	}
	public void setClearzero(boolean clearzero) {
		this.clearzero = clearzero;
	}
	public String getChartype() {
		return chartype;
	}
	public void setChartype(String chartype) {
		this.chartype = chartype;
	}
	@Transient
	public SearchResultTemplet getTemplet(){
		SearchResultTemplet templet = null ;
		if(this.chartemplet!=null){
			templet = (SearchResultTemplet) CacheHelper.getDistributedDictionaryCacheBean().getCacheObject(this.chartemplet, this.orgi) ;
		}
		return templet ;
	}
	public StyleBean getOddstyle() {
		return oddstyle;
	}
	public void setOddstyle(StyleBean oddstyle) {
		this.oddstyle = oddstyle;
	}
	public StyleBean getEvenstyle() {
		return evenstyle;
	}
	public void setEvenstyle(StyleBean evenstyle) {
		this.evenstyle = evenstyle;
	}
	public StyleBean getStyle() {
		return style;
	}
	public void setStyle(StyleBean style) {
		this.style = style;
	}
	@Transient
	public List<Warning> getWarning() {
		return warning!=null ? warning : (warning = new ArrayList<Warning>());
	}
	public void setWarning(List<Warning> warning) {
		this.warning = warning;
	}
	
	@Transient
	public String getFormatQueryText(){
		String formatSQL = null ;
		if(this.querytext!=null && this.querytext.length()>0){
			formatSQL = RivuTools.formatQuery(this.querytext) ;
		}
		return formatSQL ;
	}
	
	@Transient
	public List<ModelPackage> getModelPackageList() {
		return modelPackageList!=null ? modelPackageList : (modelPackageList = new ArrayList<ModelPackage>());
	}
	public void setModelPackageList(List<ModelPackage> modelPackageList) {
		this.modelPackageList = modelPackageList;
	}
	public Cube getCube() {
		return cube;
	}
	public void setCube(Cube cube) {
		this.cube = cube;
	}
	public String getPosx() {
		return posx;
	}
	public void setPosx(String posx) {
		this.posx = posx;
	}
	public String getPosy() {
		return posy;
	}
	public void setPosy(String posy) {
		this.posy = posy;
	}
	public String getPoswidth() {
		return poswidth;
	}
	public void setPoswidth(String poswidth) {
		this.poswidth = poswidth;
	}
	public String getPosheight() {
		return posheight;
	}
	public void setPosheight(String posheight) {
		this.posheight = posheight;
	}
	public String getWidthunit() {
		return widthunit;
	}
	public void setWidthunit(String widthunit) {
		this.widthunit = widthunit;
	}
	public String getHeightunit() {
		return heightunit;
	}
	public void setHeightunit(String heightunit) {
		this.heightunit = heightunit;
	}
	
	public boolean isExpandbtm() {
		return expandbtm;
	}
	public void setExpandbtm(boolean expandbtm) {
		this.expandbtm = expandbtm;
	}
	public boolean isExpandrgt() {
		return expandrgt;
	}
	public void setExpandrgt(boolean expandrgt) {
		this.expandrgt = expandrgt;
	}
	public String getCurtab() {
		return curtab;
	}
	public void setCurtab(String curtab) {
		this.curtab = curtab;
	}
	@Transient
	public ExportFile getExport() {
		return export;
	}
	public void setExport(ExportFile export) {
		this.export = export;
	}
	
	@Transient
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getDefheight() {
		return defheight;
	}
	public void setDefheight(String defheight) {
		this.defheight = defheight;
	}
	public String getDefwidth() {
		return defwidth;
	}
	public void setDefwidth(String defwidth) {
		this.defwidth = defwidth;
	}
	public String getHiddencolstr() {
		return hiddencolstr;
	}
	public void setHiddencolstr(String hiddencolstr) {
		this.hiddencolstr = hiddencolstr;
	}
	@Transient
	public void setColSize(int colSize) {
		this.colSize = colSize;
	}
	public int getColSize() {
		return colSize;
	}
	@Transient
	public String getExporttitle() {
		return exporttitle;
	}
	public void setExporttitle(String exporttitle) {
		this.exporttitle = exporttitle;
	}
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getMicrometercolstr() {
		return micrometercolstr;
	}
	public void setMicrometercolstr(String micrometercolstr) {
		this.micrometercolstr = micrometercolstr;
	}
}