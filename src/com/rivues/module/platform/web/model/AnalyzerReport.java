package com.rivues.module.platform.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Transient;

public class AnalyzerReport implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date createtime = new Date();
	private String html ;
	private String reportname ;
	private List<AnalyzerReportModel> model = new ArrayList<AnalyzerReportModel>();
	private Set<ModelContainer> container ;
	private String publishtype ;
	@Transient
	private List<String> idlist;
	private List<ReportFilter> filters = new ArrayList<ReportFilter>() ;
	private Map<String , Object> requestParamValues = null ;
	private Map<String , Object> hiddenParamValues = new HashMap<String, Object>() ;
	private boolean lazy ;
	private boolean loadata ;
	
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public List<AnalyzerReportModel> getModel() {
		return model;
	}
	public void setModel(List<AnalyzerReportModel> model) {
		this.model = model;
	}
	public Set<ModelContainer> getContainer() {
		return container;
	}
	public void setContainer(Set<ModelContainer> container) {
		this.container = container;
	}
	public String getPublishtype() {
		return publishtype;
	}
	public void setPublishtype(String publishtype) {
		this.publishtype = publishtype;
	}
	public List<String> getIdlist() {
		return idlist;
	}
	public void setIdlist(List<String> idlist) {
		this.idlist = idlist;
	}
	public List<ReportFilter> getFilters() {
		return filters!=null ? filters : (filters = new ArrayList<ReportFilter>());
	}
	public void setFilters(List<ReportFilter> filters) {
		this.filters = filters;
	}
	public Map<String, Object> getRequestParamValues() {
		return requestParamValues;
	}
	public void setRequestParamValues(Map<String, Object> requestParamValues) {
		this.requestParamValues = requestParamValues;
	}
	public Map<String, Object> getHiddenParamValues() {
		return hiddenParamValues == null ? new HashMap() : hiddenParamValues;
	}
	public void setHiddenParamValues(Map<String, Object> hiddenParamValues) {
		this.hiddenParamValues = hiddenParamValues;
	}
	public boolean isLazy() {
		return lazy;
	}
	public void setLazy(boolean lazy) {
		this.lazy = lazy;
	}
	public boolean isLoadata() {
		return loadata;
	}
	public void setLoadata(boolean loadata) {
		this.loadata = loadata;
	}
	public String getReportname() {
		return reportname;
	}
	public void setReportname(String reportname) {
		this.reportname = reportname;
	}
}
