/**
 * Licensed to the Rivulet under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     webapps/LICENSE-Rivulet-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rivues.module.platform.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author jaddy0302 Rivulet PluginParam.java 2010-4-15
 * 修改用处，作为角色授权记录表
 * 
 */
@Entity
@Table(name = "rivu5_pluginparam")
@org.hibernate.annotations.Proxy(lazy = false)
public class PluginParam implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2994382274287184346L;
	private String id ;
	private String tpid ;
	private String pluginid ;
	private String name ;
	private String value ;
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the tpid
	 */
	public String getTpid() {
		return tpid;
	}
	/**
	 * @param tpid the tpid to set
	 */
	public void setTpid(String tpid) {
		this.tpid = tpid;
	}
	/**
	 * @return the pluginid
	 */
	public String getPluginid() {
		return pluginid;
	}
	/**
	 * @param pluginid the pluginid to set
	 */
	public void setPluginid(String pluginid) {
		this.pluginid = pluginid;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
