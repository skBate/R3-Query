package com.rivues.module.platform.web.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rivu5_auth")
@org.hibernate.annotations.Proxy(lazy = false)
public class Auth implements java.io.Serializable {
	/**
	 * vid
	 */
	private static final long serialVersionUID = 2956015164694993514L;
	private String id;
	private String resourceid;//资源id
	private String resourcetype;//资源类型 1报表 5目录 3:功能菜单,2 cube(模型) ,4 measure（指标） 6 level（维度成员）
	private String resourcedic;//是否为目录
	private String authtype;//权限分类 
	private String authread;//单独分出字段，读权限1，无权限0
	private String ownerid;//权限所有者id(角色id/组织id/用户id)
	private String ownertype;//权限对象     1 角色  2组织机构  3个人
	private String ownername;//所有者名称  角色名/组织机构名/用户姓名
	private String dataid ;	//数据模型的 ID ， 指标共享
	
	private String orgi;//租户
	private Date createtime = new Date();//创建时间
	
	private boolean parent = false ;		//在指标字典中使用的时候标示是否是继承的权限
	private String parentdic ;				//在指标字典中使用的时候标示继承自父类的目录名称
	private String parentdicid ;			//在指标字典中使用的时候标示继承自父类的目录ID
	
	//权限归属者类型   角色
	@Transient 
	public static final String OWNER_TYPE_ROLE="1";
	//权限归属者类型   角色分组
	@Transient 
	public static final String OWNER_TYPE_ROLE_GROUP="4";
	//权限归属者类型   组织机构
	@Transient
	public static final String OWNER_TYPE_ORGAN="2";
	//权限归属者类型   用户
	@Transient
	public static final String OWNER_TYPE_USER="3";

	
	@Transient
	// 资源类型， 报表
	public static final String RESOURCE_TYPE_REPORT = "1";
	
	@Transient
	public static final String RESOURCE_TYPE_DIC = "5";
	// 资源类型， 模型
	@Transient
	public static final String RESOURCE_TYPE_MODEL = "2";
	
	@Transient
	public static final String RESOURCE_TYPE_MEASURE = "4";
	
	@Transient
	public static final String RESOURCE_TYPE_MENU = "3";

	// 资源类型， 维度成员
	@Transient
	public static final String RESOURCE_TYPE_LEVEL = "6";
	
	

	// 是否为目录 是
	@Transient
	public static final String RESOURCE_DIC_Y = "1";

	// 是否为目录 否
	@Transient
	public static final String RESOURCE_DIC_N = "0";
	
	
	//操作权限 读
	@Transient
	public static final String AUTH_READ="1";
	
	
	//禁止操作
	@Transient
	public static final String AUTH_NONE="0";
	
	//操作权限 写
	@Transient
	public static final String AUTH_TYPE_EDIT="2";
	
	//操作权限 设置策略
	@Transient
	public static final String AUTH_TYPE_SET="3";
	
	//操作权限 设计
	@Transient
	public static final String AUTH_TYPE_DESIGN="4";
	
	//操作权限 执行
	@Transient
	public static final String AUTH_TYPE_ACTION="5";
	
	//操作权限 遍历
	@Transient
	public static final String AUTH_TYPE_LIST="6";
	
	//目录操作权限 导入
	@Transient
	public static final String AUTH_TYPE_IMPORT="7";
	@Transient
	//目录操作权限 计划任务
	public static final String AUTH_TYPE_PLAN="8";
	
 
	@Transient
	public List<String> getAuthList(){
		return StringUtils.isEmpty(getAuthtype())? new ArrayList<String>() : Arrays.asList(getAuthtype().split(","));
	}
	
	@Transient
	public static List<String> getAllReportAuth(){
		List<String> list = new ArrayList<String>();
		list.add(AUTH_TYPE_ACTION);
		list.add(AUTH_TYPE_LIST);
		list.add(AUTH_TYPE_DESIGN);
		list.add(AUTH_TYPE_SET);
		list.add(AUTH_TYPE_EDIT);
		list.add(AUTH_READ);
		return list;
 	}
	
	
	@Transient
	public static List<String> getAllDicAuth(){
		List<String> list = new ArrayList<String>();
		list.add(AUTH_TYPE_ACTION);
		list.add(AUTH_TYPE_LIST);
		list.add(AUTH_TYPE_DESIGN);
		list.add(AUTH_TYPE_SET);
		list.add(AUTH_TYPE_EDIT);
		list.add(AUTH_READ);
		return list;
 	}
	
	
	

	public String getAuthread() {
		return authread;
	}

	public void setAuthread(String authread) {
		this.authread = authread;
	}

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResourceid() {
		return resourceid;
	}

	public void setResourceid(String resourceid) {
		this.resourceid = resourceid;
	}

	public String getResourcetype() {
		return resourcetype;
	}

	public void setResourcetype(String resourcetype) {
		this.resourcetype = resourcetype;
	}

	public String getResourcedic() {
		return resourcedic;
	}

	public void setResourcedic(String resourcedic) {
		this.resourcedic = resourcedic;
	}

	public String getAuthtype() {
		return authtype;
	}

	public void setAuthtype(String authtype) {
		this.authtype = authtype;
	}



	public String getOwnerid() {
		return ownerid;
	}



	public void setOwnerid(String ownerid) {
		this.ownerid = ownerid;
	}



	public String getOwnertype() {
		return ownertype;
	}



	public void setOwnertype(String ownertype) {
		this.ownertype = ownertype;
	}


	@Transient
	public String getOwnername() {
		return ownername!=null?ownername:ownerid;
	}



	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}



	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getDataid() {
		return dataid;
	}

	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	@Transient
	public boolean isParent() {
		return parent;
	}
	
	public void setParent(boolean parent) {
		this.parent = parent;
	}
	@Transient
	public String getParentdic() {
		return parentdic;
	}

	public void setParentdic(String parentdic) {
		this.parentdic = parentdic;
	}
	@Transient
	public String getParentdicid() {
		return parentdicid;
	}

	public void setParentdicid(String parentdicid) {
		this.parentdicid = parentdicid;
	}
	
}
