package com.rivues.module.platform.web.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rivu5_analyzereportpagesetting")
@org.hibernate.annotations.Proxy(lazy = false)
public class AnalyzerReportPageSetting implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4611750881125888452L;
	private String id;
	private String height;
	private String backcolor;
	private String font;
	private String titlecolor;
	private String width;
	private String backstyle;
	private String widthcolor;
	private Map  widths = new HashMap();
	private Map  widthcolors = new HashMap();
	

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getBackcolor() {
		return backcolor;
	}
	public void setBackcolor(String backcolor) {
		this.backcolor = backcolor;
	}
	public String getFont() {
		return font;
	}
	public void setFont(String font) {
		this.font = font;
	}
	public String getTitlecolor() {
		return titlecolor;
	}
	public void setTitlecolor(String titlecolor) {
		this.titlecolor = titlecolor;
	}
	public String getBackstyle() {
		return backstyle;
	}
	public void setBackstyle(String backstyle) {
		this.backstyle = backstyle;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	@Transient
	public Map getWidths() {
		return widths;
	}
	public void setWidths(Map widths) {
		this.widths = widths;
	}
	public String getWidthcolor() {
		return widthcolor;
	}
	public void setWidthcolor(String widthcolor) {
		this.widthcolor = widthcolor;
	}
	@Transient
	public Map getWidthcolors() {
		return widthcolors;
	}
	public void setWidthcolors(Map widthcolors) {
		this.widthcolors = widthcolors;
	}
}
