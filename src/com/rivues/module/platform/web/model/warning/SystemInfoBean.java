package com.rivues.module.platform.web.model.warning;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.rivues.module.platform.web.model.MonitorBean;

@Entity
@Table(name = "rivu5_log_systeminfo")
@org.hibernate.annotations.Proxy(lazy = false)
public class SystemInfoBean extends MonitorBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8751873703503398934L;
	private String id ;
	private Date createdate = new Date();	//创建时间
	private int dataflag = (int)((System.currentTimeMillis()/1000)/5);		//时间标记
	private long unavailabletime ;	//系统不可用时间
	private String unavailabletype;	//服务不可用类型
	private long serverdowntime ;	//服务器DOWN 时间
	private String hostname;	//服务器主机名
	private int port;		//服务器端口
	private String ipaddr ;		//服务器地址
	private String groupid ;	//服务组别
	private String datatype;	//监控信息产生类型：auto
	private double cpuus ;			//CPU使用率中 用户CPU使用率
	private double cpusys	;		//CPU使用率 中系统CPU使用率
	private double cpuidle ;		//CPU空闲率
	private double cpuwait ;		//CPU等待执行比率
	private double cpunice ;		//
	private double cpupercent;		//CPU利用率
	
	
	private long memav ;			//内存平均
	private double mempercent ; 	//内存使用比率
	private long memtotal 	;		//物理内存总数
	private long memused;		//物理内存- 已使用内存
	private long memfree;		//物理内存- 空闲内存
	
	private long jvmmemtotal  ;
	private long jvmmemfree;
	private double jvmmemusepercent;
	private long jvmmemuse ;
	
	
	private long diskfree;		//剩余存储空间
	private long diskuse ;		//当前目录剩余空间
	private long tempdiskfree ;	//临时文件夹 剩余空间
	private long tempdisk ; 	//临时文件夹总空间
	private double tempdiskpercent ;//临时文件夹 空间剩余率 
	
	private double diskusepercent ;	//磁盘空闲存储比例
	private long disktotal;		//磁盘总存储空间
	private long diskread ;		//磁盘总读取字节数
	private long diskwrite;		//磁盘总写入数据字节数
	private double diskqueue;		//磁盘等待队列
	private double diskwritespeed ;	//磁盘写入速率
	private double diskreadspeed ;	//磁盘读取速率
	
	private long swaptotal ;		//交换分区
	private long swapuse ;			//已使用交换分区
	private long swapfree ;			//剩余交换分区
	private double swappercent ;	//交换分区使用比率
	private double swappagein	;			//操作系统的 内存交换 Page IN
	private double swappageout;			//操作系统的内存交换 Page Out
	
	
	private long netspeed ;		//网络速率 ，例如：全双工 100M
	private long netread ;		//网络读取总量
	private long netwrite ; 	//网络写入总量
	private long netreadspeed;	//网络读取流量
	private long netwritespeed;	//网络写入流量
	private double netuseprecent;	//网络利用率
	
	private String netname ;	//网络写入设备名称， eth0 , eth1
	private long rxpackage;		//收取到的包数量
	private long txpackage ;	//发送的包数量
	private double rxpackagespeed ;	//收包速率
	private double txpackagespeed ;	//发包速率
	private long rxerrors;		//收到的错误包数量
	private long txerrors;		//发送的错误包数量
	private String triggerwarning ; //触发预警服务
	private String   triggertime ;// 触发预警服务时间
	private long uptime ;			//系统启动时长
	private Date updatetime ;		//系统启动时间
	private String hardware ; 		//硬件类型： 64位 ， 32位
	private String ostype ; 		//操作系统 
	 
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getDatatype() {
		return datatype;
	}
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	public double getCpuus() {
		return cpuus;
	}
	public void setCpuus(double cpuus) {
		this.cpuus = cpuus;
	}
	public double getCpusys() {
		return cpusys;
	}
	public void setCpusys(double cpusys) {
		this.cpusys = cpusys;
	}
	public double getCpuidle() {
		return cpuidle;
	}
	public void setCpuidle(double cpuidle) {
		this.cpuidle = cpuidle;
	}
	public double getCpuwait() {
		return cpuwait;
	}
	public void setCpuwait(double cpuwait) {
		this.cpuwait = cpuwait;
	}
	public double getCpunice() {
		return cpunice;
	}
	public void setCpunice(double cpunice) {
		this.cpunice = cpunice;
	}
	public void setCpunice(int cpunice) {
		this.cpunice = cpunice;
	}
	public long getMemav() {
		return memav;
	}
	public void setMemav(long memav) {
		this.memav = memav;
	}
	public long getMemused() {
		return memused;
	}
	public void setMemused(long memused) {
		this.memused = memused;
	}
	public long getMemfree() {
		return memfree;
	}
	public void setMemfree(long memfree) {
		this.memfree = memfree;
	}
	public long getDiskfree() {
		return diskfree;
	}
	public void setDiskfree(long diskfree) {
		this.diskfree = diskfree;
	}
	public long getDiskuse() {
		return diskuse;
	}
	public void setDiskuse(long diskuse) {
		this.diskuse = diskuse;
	}
	public double getDiskusepercent() {
		return diskusepercent;
	}
	public void setDiskusepercent(double diskusepercent) {
		this.diskusepercent = diskusepercent;
	}
	public long getDisktotal() {
		return disktotal;
	}
	public void setDisktotal(long disktotal) {
		this.disktotal = disktotal;
	}
	public long getDiskread() {
		return diskread;
	}
	public void setDiskread(long diskread) {
		this.diskread = diskread;
	}
	public long getDiskwrite() {
		return diskwrite;
	}
	public void setDiskwrite(long diskwrite) {
		this.diskwrite = diskwrite;
	}
	
	public double getDiskqueue() {
		return diskqueue;
	}
	public void setDiskqueue(double diskqueue) {
		this.diskqueue = diskqueue;
	}
	public long getNetread() {
		return netread;
	}
	public void setNetread(long netread) {
		this.netread = netread;
	}
	public long getNetwrite() {
		return netwrite;
	}
	public void setNetwrite(long netwrite) {
		this.netwrite = netwrite;
	}
	
	public long getNetreadspeed() {
		return netreadspeed;
	}
	public void setNetreadspeed(long netreadspeed) {
		this.netreadspeed = netreadspeed;
	}
	public long getNetwritespeed() {
		return netwritespeed;
	}
	public void setNetwritespeed(long netwritespeed) {
		this.netwritespeed = netwritespeed;
	}
	public String getNetname() {
		return netname;
	}
	public void setNetname(String netname) {
		this.netname = netname;
	}
	public double getMempercent() {
		return mempercent;
	}
	public void setMempercent(double mempercent) {
		this.mempercent = mempercent;
	}
	public long getMemtotal() {
		return memtotal;
	}
	public void setMemtotal(long memtotal) {
		this.memtotal = memtotal;
	}
	public long getRxpackage() {
		return rxpackage;
	}
	public void setRxpackage(long rxpackage) {
		this.rxpackage = rxpackage;
	}
	public long getTxpackage() {
		return txpackage;
	}
	public void setTxpackage(long txpackage) {
		this.txpackage = txpackage;
	}
	public long getRxerrors() {
		return rxerrors;
	}
	public void setRxerrors(long rxerrors) {
		this.rxerrors = rxerrors;
	}
	public long getTxerrors() {
		return txerrors;
	}
	public void setTxerrors(long txerrors) {
		this.txerrors = txerrors;
	}
	public int getDataflag() {
		return dataflag;
	}
	public void setDataflag(int dataflag) {
		this.dataflag = dataflag;
	}
	public long getJvmmemtotal() {
		return jvmmemtotal;
	}
	public void setJvmmemtotal(long jvmmemtotal) {
		this.jvmmemtotal = jvmmemtotal;
	}
	public long getJvmmemfree() {
		return jvmmemfree;
	}
	public void setJvmmemfree(long jvmmemfree) {
		this.jvmmemfree = jvmmemfree;
	}
	public double getJvmmemusepercent() {
		return jvmmemusepercent;
	}
	public void setJvmmemusepercent(double jvmmemusepercent) {
		this.jvmmemusepercent = jvmmemusepercent;
	}
	public long getJvmmemuse() {
		return jvmmemuse;
	}
	public void setJvmmemuse(long jvmmemuse) {
		this.jvmmemuse = jvmmemuse;
	}
	public String getTriggerwarning() {
		return triggerwarning;
	}
	public void setTriggerwarning(String triggerwarning) {
		this.triggerwarning = triggerwarning;
	}
	public String getTriggertime() {
		return triggertime;
	}
	public void setTriggertime(String triggertime) {
		this.triggertime = triggertime;
	}
	public long getNetspeed() {
		return netspeed;
	}
	public void setNetspeed(long netspeed) {
		this.netspeed = netspeed;
	}
	public double getNetuseprecent() {
		return netuseprecent;
	}
	public void setNetuseprecent(double netuseprecent) {
		this.netuseprecent = netuseprecent;
	}
	public double getCpupercent() {
		return cpupercent;
	}
	public void setCpupercent(double cpupercent) {
		this.cpupercent = cpupercent;
	}
	public long getUptime() {
		return uptime;
	}
	public void setUptime(long uptime) {
		this.uptime = uptime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getHardware() {
		return hardware;
	}
	public void setHardware(String hardware) {
		this.hardware = hardware;
	}
	public String getOstype() {
		return ostype;
	}
	public void setOstype(String ostype) {
		this.ostype = ostype;
	}
	public double getRxpackagespeed() {
		return rxpackagespeed;
	}
	public void setRxpackagespeed(double rxpackagespeed) {
		this.rxpackagespeed = rxpackagespeed;
	}
	public double getTxpackagespeed() {
		return txpackagespeed;
	}
	public void setTxpackagespeed(double txpackagespeed) {
		this.txpackagespeed = txpackagespeed;
	}
	public double getDiskwritespeed() {
		return diskwritespeed;
	}
	public void setDiskwritespeed(double diskwritespeed) {
		this.diskwritespeed = diskwritespeed;
	}
	public double getDiskreadspeed() {
		return diskreadspeed;
	}
	public void setDiskreadspeed(double diskreadspeed) {
		this.diskreadspeed = diskreadspeed;
	}
	public long getTempdiskfree() {
		return tempdiskfree;
	}
	public void setTempdiskfree(long tempdiskfree) {
		this.tempdiskfree = tempdiskfree;
	}
	public long getTempdisk() {
		return tempdisk;
	}
	public void setTempdisk(long tempdisk) {
		this.tempdisk = tempdisk;
	}
	public double getTempdiskpercent() {
		return tempdiskpercent;
	}
	public void setTempdiskpercent(double tempdiskpercent) {
		this.tempdiskpercent = tempdiskpercent;
	}
	public long getSwaptotal() {
		return swaptotal;
	}
	public void setSwaptotal(long swaptotal) {
		this.swaptotal = swaptotal;
	}
	public long getSwapuse() {
		return swapuse;
	}
	public void setSwapuse(long swapuse) {
		this.swapuse = swapuse;
	}
	public long getSwapfree() {
		return swapfree;
	}
	public void setSwapfree(long swapfree) {
		this.swapfree = swapfree;
	}
	public double getSwappercent() {
		return swappercent;
	}
	public void setSwappercent(double swappercent) {
		this.swappercent = swappercent;
	}
	public double getSwappagein() {
		return swappagein;
	}
	public void setSwappagein(double swappagein) {
		this.swappagein = swappagein;
	}
	public double getSwappageout() {
		return swappageout;
	}
	public void setSwappageout(double swappageout) {
		this.swappageout = swappageout;
	}
	public long getUnavailabletime() {
		return unavailabletime;
	}
	public void setUnavailabletime(long unavailabletime) {
		this.unavailabletime = unavailabletime;
	}
	public String getUnavailabletype() {
		return unavailabletype;
	}
	public void setUnavailabletype(String unavailabletype) {
		this.unavailabletype = unavailabletype;
	}
	public long getServerdowntime() {
		return serverdowntime;
	}
	public void setServerdowntime(long serverdowntime) {
		this.serverdowntime = serverdowntime;
	}
}
