/**
 * Licensed to the Rivulet under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     webapps/LICENSE-Rivulet-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rivues.module.platform.web.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.apache.solr.client.solrj.beans.Field;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 * @author jaddy0302 Rivulet User.java 2010-3-17
 * 
 */
@Entity
@Table(name = "rivu5_user")
@org.hibernate.annotations.Proxy(lazy = false)
public class User implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4921506359981218058L;
	@Field
	private String id ;
	
	private String sessionid ;
	
	private String username ;
	private String password ;
	private String email ;
	private String firstname ;
	private String midname ;
	private String lastname ;
	private String language ;
	private String jobtitle ;
	private String department ;
	private String gender;
	private String birthday ;
	private String nickname ;
	private String secureconf = "5";
	private String usertype ; // 0 Admin User  : !0  Other User 
	private String rulename ;
	private String searchprojectid ;
	private String type ;
	private Map property ;
	private boolean login = false ;
	private String loginType = "ESP-R3" ;
	private Date loginTime = new Date() ;
	private String orgi ;
	private String creater;
	private Date createtime;
	private Date passupdatetime;
	private Date updatetime;
	private String memo;
	private String organ;
	private List<Auth> authList = new ArrayList<Auth>();
	private List<Role> roleList = new ArrayList<Role>();
	private List<Organ> organList = new ArrayList<Organ>();
	
	
	private String newpwd;
	// 新密码
	
	//电话
	private String mobile;
	
	@Transient
	public String getNewpwd() {
		return newpwd;
	}

	public void setNewpwd(String newpwd) {
		this.newpwd = newpwd;
	}
	
	
	
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password!=null?password:"";
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the midname
	 */
	public String getMidname() {
		return midname;
	}
	/**
	 * @param midname the midname to set
	 */
	public void setMidname(String midname) {
		this.midname = midname;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return the jobtitle
	 */
	public String getJobtitle() {
		return jobtitle;
	}
	/**
	 * @param jobtitle the jobtitle to set
	 */
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}
	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return birthday;
	}
	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * @return the secureconf
	 */
	public String getSecureconf() {
		return secureconf;
	}
	/**
	 * @param secureconf the secureconf to set
	 */
	public void setSecureconf(String secureconf) {
		this.secureconf = secureconf;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getRulename() {
		return rulename;
	}
	public void setRulename(String rulename) {
		this.rulename = rulename;
	}
	public String getSearchprojectid() {
		return searchprojectid;
	}
	public void setSearchprojectid(String searchprojectid) {
		this.searchprojectid = searchprojectid;
	}
	@Transient
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Transient
	public Map getProperty() {
		return property;
	}
	public void setProperty(Map property) {
		this.property = property;
	}
	@Transient
	public boolean isTemp(boolean temp){
		return temp ;
	}
	
	@Transient
	public boolean isLogin() {
		return login;
	}
	@Transient
	public void setLogin(boolean login) {
		this.login = login;
	}
	@Transient
	public String getLoginType() {
		return loginType;
	}
	@Transient
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	@Transient
	public Date getLoginTime() {
		return loginTime;
	}
	@Transient
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Transient
	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	@Transient
	public boolean isAllow(String code){
		boolean allow = false ;
		if(this.authList!=null && this.authList.size()>0){
			for(Auth auth : this.authList){
				if(auth.getDataid()!=null && auth.getDataid().equals(code)){
					allow = true ;
					break ;
				}
			}
		}
		return "0".equals(this.usertype) ? true : allow ;
	}
	@Transient
	public List<Auth> getAuthList() {
		return authList;
	}

	public void setAuthList(List<Auth> authList) {
		this.authList = authList;
	}
	@Transient
	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
	@Transient
	public List<Organ> getOrganList() {
		return organList;
	}

	public void setOrganList(List<Organ> organList) {
		this.organList = organList;
	}

	public String getOrgan() {
		return organ;
	}

	public void setOrgan(String organ) {
		this.organ = organ;
	}

	public Date getPassupdatetime() {
		return passupdatetime;
	}

	public void setPassupdatetime(Date passupdatetime) {
		this.passupdatetime = passupdatetime;
	}
	
}
