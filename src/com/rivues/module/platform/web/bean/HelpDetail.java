package com.rivues.module.platform.web.bean;

import java.util.Date;

import org.apache.solr.client.solrj.beans.Field;

public class HelpDetail implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Field
	private String id;
	@Field
	private String name;
	@Field
	private String sort;
	@Field
	private String content;
	@Field
	private String description;
	@Field
	private Date createtime;
	@Field
	private Date updatetime;
	@Field
	private String groupid;
	@Field
	private String clazz=this.getClass().getName();
	@Field
	private String creater;
	
	
	private HelpGroup group;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	public HelpGroup getGroup() {
		return group;
	}
	public void setGroup(HelpGroup group) {
		this.group = group;
	}

}
