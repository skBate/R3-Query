package com.rivues.module.platform.web.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rivues.module.platform.web.interceptor.LogIntercreptorHandler;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.module.platform.web.model.CubeMeasure;
import com.rivues.module.platform.web.model.CubeMetadata;
import com.rivues.module.platform.web.model.Database;
import com.rivues.module.platform.web.model.Dimension;
import com.rivues.module.platform.web.model.PublishedCube;
import com.rivues.module.platform.web.model.User;
import com.rivues.module.platform.web.model.UserOrgan;
import com.rivues.module.report.web.model.PublishedReport;
import com.rivues.util.RivuTools;
import com.rivues.util.iface.authz.UserAuthzFactory;
import com.rivues.util.iface.report.ReportFactory;
import com.rivues.util.local.LocalTools;
import com.rivues.util.serialize.JSON;

@Controller  
@RequestMapping("/{orgi}/api/user")  
public class UserAPIController extends Handler{
	private final Logger log = LoggerFactory.getLogger(UserAPIController.class); 
	/**
	 * 
	 * @param request
	 * @param response
	 * @param orgi
	 * @throws IOException
	 */
    @RequestMapping(value="/useraddo" , name="useraddo" , type="api",subtype="user")
	public void getDBSourceListJSON(HttpServletRequest request ,HttpServletResponse response,@Valid String authzUser,@Valid String authzPassword,@PathVariable String orgi,@Valid User user) throws Exception {
    	int count = super.getService().getCountByCriteria(DetachedCriteria.forClass(User.class).add(Restrictions.eq("username", user.getUsername())).add(Restrictions.eq("orgi", orgi))) ;
    	user.setOrgi(orgi);
    	boolean authz = UserAuthzFactory.getInstance(orgi).authentication(authzUser,authzPassword,orgi) ;
    	response.setContentType("text/html;charset=UTF-8");
    	PrintWriter writer = response.getWriter();
    	String result = null;
    	if(!authz){
    		result = "authentication_faild";
    	}else if(count>0){
    		result = "usersize:"+String.valueOf(count);
    	}else{
    		super.getService().saveIObject(user);
    		//默认没有分组建立一个为零的关系（也就是在默认组里）
        	UserOrgan ou = new UserOrgan();
        	ou.setOrganid("0");
        	ou.setUserid(user.getId());
        	ou.setOrgi(orgi);
        	super.getService().saveIObject(ou);
    		result = user.getId();
    	}
    	writer.write(result);
	}
}


























