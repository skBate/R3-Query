package com.rivues.module.jobs.plugin;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ReportJob implements Job{
	/**
	 * 报表ID
	 */
	private String id ;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("Running......");
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
