package com.rivues.module.report.web.model;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.util.RivuTools;
import com.rivues.util.tools.FileUtil;

public class MailTask implements EventTask{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1556212434060622351L;
	private String to ;
	private String cc;
	private String title ;
	private String content ;
	private String isAppendix;//是否附件发送
	/* (non-Javadoc)
	 * @see com.rivues.module.report.web.model.EventTaskInterface#getTo()
	 */
	@Override
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	/* (non-Javadoc)
	 * @see com.rivues.module.report.web.model.EventTaskInterface#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	/* (non-Javadoc)
	 * @see com.rivues.module.report.web.model.EventTaskInterface#getContent()
	 */
	@Override
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getIsAppendix() {
		return isAppendix;
	}
	public void setIsAppendix(String isAppendix) {
		this.isAppendix = isAppendix;
	}
	@Override
	public boolean process(TaskReportInfo taskReportInfo,JobDetail job) throws Exception {
		/**
		 * 首先，解析出来需要替换的正文内容中的 参数，然后通过解析参数格式，获取数据，并使用数据填充
		 * 解析方式：使用正则解析？
		 */
		
		//处理附件（存临时文件到系统中）
		
		List<String> filenames =  null;
		if(!StringUtils.isBlank(this.isAppendix)){//如果没选择附件发送则不发送附件
			filenames = RivuTools.processAppendix(taskReportInfo,job);
		}
		try{
			RivuTools.sendMail(this.to , this.cc, RivuTools.processContent(this.title, taskReportInfo , false) , RivuTools.processContent(content, taskReportInfo , true),filenames) ;
		}finally{
			//邮件发送完毕删除临时文件
			FileUtil.deleteFiles(filenames);
		}
		return true ;
	}

	
}
