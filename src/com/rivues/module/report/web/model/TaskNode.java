package com.rivues.module.report.web.model;

import java.util.Date;

public class TaskNode implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2942499979967710602L;
	private String id = String.valueOf(System.currentTimeMillis());
	private String type ;
	private Date cretetime = new Date();
	private Object taskConfig ;
	public TaskNode(){}
	
	public TaskNode(String type , Object taskConfig){
		this.type = type ;
		this.taskConfig = taskConfig ;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getCretetime() {
		return cretetime;
	}
	public void setCretetime(Date cretetime) {
		this.cretetime = cretetime;
	}

	public Object getTaskConfig() {
		return taskConfig;
	}

	public void setTaskConfig(Object taskConfig) {
		this.taskConfig = taskConfig;
	}
	
}
