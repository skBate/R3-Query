package com.rivues.module.report.web.model;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.aviator.AviatorEvaluator;
import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.util.RivuTools;

public class RouteTask implements EventTask{
	private final Logger log = LoggerFactory.getLogger(RouteTask.class); 
	/**
	 * 
	 */
	private static final long serialVersionUID = -1556212434060622351L;
	private String to ;
	private String title ;
	private String content ;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public boolean process(TaskReportInfo taskReportInfo,JobDetail job) throws Exception {
		boolean result = false ;
		Object value = null ;
		try{
			if(this.content!=null && this.content.length()>0){
				value = AviatorEvaluator.execute(RivuTools.processContent(this.content, taskReportInfo , false) , new HashMap<String , Object>()) ;
				if(value instanceof Boolean){
					result = (Boolean)value ;
				}else if(value instanceof Number){
					result = ((Number)value).intValue() != 0 ;
				}else if(value instanceof String){
					result = !((String)value).equals("0") ;
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return result ;
	}
	@Override
	public String getIsAppendix() {
		// TODO Auto-generated method stub
		return null;
	}
	public static void main(String[] args){
		Object value = AviatorEvaluator.execute("" , new HashMap<String , Object>()) ;
		System.out.println(value) ;
	}
	
}
