package com.rivues.module.report.web.model;

import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.util.RivuTools;
import com.rivues.util.sms.MessageFactory;
import com.rivues.util.sms.MessageSend;

public class SMSTask implements EventTask{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1556212434060622351L;
	private String to ;
	private String title ;
	private String content ;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String getIsAppendix() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean process(TaskReportInfo taskReportInfo,JobDetail job) throws Exception {
		/**
		 * 发送短信
		 */
		RivuTools.sendMessage(this.to, RivuTools.processContent(this.content, taskReportInfo , false),job);
		/**
		 * 调用发送短信的接口， 短信接口的配置在 后台 系统管理->通知管理->短信网关
		 */
		return true ;
	}
	
}
